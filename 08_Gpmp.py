import matplotlib.pyplot as plt
import matplotlib as mpl
import os, sys
import numpy as np
import constants
from util import find_Gpmp

plt.rc('font', family='TeX Gyre Schola')

FILENAME = os.path.splitext(__file__)[0]

#################################
## some constants for glacier ice
lapse_rate     = constants.lapse_rate     # degrees per meter
conductivity   = constants.conductivity   # W/m/K
diffusivity    = constants.diffusivity    # m2/yr
surface_temp   = constants.surface_temp   # K
rho            = constants.rho            # kg/m3
gravity        = constants.gravity        # m/s2
rate_factor    = constants.rate_factor    # kPa-3 yr-1
beta           = constants.beta           # K/Pa
kelvin         = constants.kelvin         # K
eps            = constants.eps            # --
heat_capacity  = constants.heat_capacity  # J/kg/K
latent_fusion  = constants.latent_fusion  # kJ/kg
y2s            = constants.y2s
n_glen         = constants.n_glen         # exponent of Glen's flow law
M_dot          = constants.M_dot          # m/yr
Length         = constants.Length         # m
numElems       = constants.numElems
GHF            = constants.GHF            # W/m2
GAMMA          = constants.GAMMA
GAMMA_OPTIMAL  = constants.GAMMA_OPTIMAL

#-----------------------------------
#G = find_Gpmp(Ts=-50., thk=1500., M_dot_srb=0.4)
#print type(G)
#sys.exit("get out of here!")
Ts_range = np.arange(-40,-10+1,10)#np.linspace(-50,-20,4)
#M_dot_srb_range = np.arange(0.05,1.01,0.05)
#thk_range = np.arange(1000,3000+1,3000)
M_dot_srb_range = np.arange(0.05,1.01,0.01)
#thk_range = np.arange(500,3000+1,500)
thk_range = [500., 1000., 1400., 1500., 2000., 2500., 3000.]

for thk in thk_range:
    print thk
    for Ts in Ts_range:
        Gs_all = []
        for M_dot_srb in M_dot_srb_range:
            Gpmp = find_Gpmp(Ts=Ts, thk=thk, M_dot_srb=M_dot_srb)
            Gs_all.append(float(Gpmp))

        kwargs = {'label':r'$T_s=%i^{\circ}$C'%Ts}
        plt.plot(M_dot_srb_range,np.asarray(Gs_all),**kwargs)

    plt.title('thickness = %i [m]'%thk)
    plt.legend(loc=2)
    plt.xlabel(r'$\dot M$ [m yr$^{-1}$]')
    plt.ylabel('$GHF_{pmp}$ [mW m$^{-2}$]')
    plt.grid(linestyle='dashed')
    plt.xlim([0,1])
    plt.ylim([20,320])
    plt.savefig("results_%s/Gpmp-thk-%i.png"%(FILENAME,thk),dpi=300)
#    plt.savefig("results_%s/test.png"%(FILENAME),dpi=300)
    plt.clf()
