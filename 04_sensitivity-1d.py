import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import constants
from dolfin import *
import numpy as np
from util import (
    SRB_solution,
    gamma_as_peclet
)

plt.rc('font', family='TeX Gyre Schola')

FILENAME = os.path.splitext(__file__)[0]

INTERVALS = 200

#######################################
M_DOTS      = np.linspace(0.05, 3, 100)
THICKNESSES = np.linspace(2000, 4000, 5)
cmap = mpl.cm.RdYlGn
for THK in THICKNESSES:
    diff_plt = []
    mesh = IntervalMesh(INTERVALS, 0, THK)
    Q    = FunctionSpace(mesh, "CG", 1)
    for M_DOT in M_DOTS:
        gamma_opt = gamma_as_peclet(M_dot=M_DOT, thk=THK)
        temperature_srb = SRB_solution(M_dot_srb=M_DOT, _gamma = gamma_opt,\
                                                    mesh=mesh, Q=Q, thk=THK)
        temperature_srb = temperature_srb.vector().array()[::-1]
        srf_bed_diff = temperature_srb[0]-temperature_srb[-1]
        diff_plt.append(srf_bed_diff)

    clr = cmap( (THK-THICKNESSES[0])/(THICKNESSES[-1]-THICKNESSES[0]) + 0.1)
    kwargs = {"color":clr, "lw":2., "alpha":1, "label":"H=%i m"%float(THK)}
    plt.plot(M_DOTS, diff_plt, **kwargs)
    plt.legend(loc=1)
    plt.grid(linestyle='dotted')

plt.xlabel(r'surface mass balance rate [m yr$^{-1}$]')
plt.ylabel(r'$T_b-T_s$ [K]')
plt.gca().set_xlim(left=0)
plt.savefig("results_%s/sensitivity.png"%FILENAME,dpi=300)
