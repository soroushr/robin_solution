import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import numpy as np
import constants
from util import find_gamma

plt.rc('font', family='TeX Gyre Schola')

FILENAME = os.path.splitext(__file__)[0]

#################################
## some constants for glacier ice
diffusivity    = constants.diffusivity
GAMMA_OPTIMAL  = constants.GAMMA_OPTIMAL
M_dot          = constants.M_dot
conductivity   = constants.conductivity

####################
# FIND OPTIMAL GAMMA


# find optimal gamma as a function of peclet
WRT_PECLET = False
if WRT_PECLET:
    M_dot_range = np.linspace(0.1,2.1,21)
    thk_range   = np.linspace(1000., 4000., 21)
    output = []
    for thk_item in thk_range:
        for M_dot_item in M_dot_range:
            gamma_opt = find_gamma(gamma_init=GAMMA_OPTIMAL, M_dot_srb=M_dot_item, THK=thk_item, TOL=0.01)
            print gamma_opt
            Peclet = thk_item*M_dot_item/diffusivity
            print Peclet
            output.append([thk_item, M_dot_item, Peclet, gamma_opt])

    np.savetxt("results_%s/outs_peclet.txt"%FILENAME, output, delimiter=',',\
                            fmt='%10.5f', header='thk, M, Pe ,gamma')

# find optimal gamma as a function of (G*H)/(kappa*T_S)
WRT_RAMIAR = True
if WRT_RAMIAR:
    M_dot_item = M_dot
    thk_range = [3000]#np.linspace(2000., 3000, 9)
    GHF_range = np.linspace(0.035, 0.065, 11)
    T_S_range = np.linspace(-50., -10., 5)
    output = []
    for thk_item in thk_range:
        for GHF_item in GHF_range:
            for T_S_item in T_S_range:
                gamma_opt = find_gamma(M_dot_srb=M_dot_item, THK=thk_item,\
                                       T_S=T_S_item, H_flux=GHF_item, TOL=0.01)
                print gamma_opt
                Ramiar = thk_item*GHF_item/(conductivity*T_S_item)
                print Ramiar
                output.append([thk_item, GHF_item, T_S_item, Ramiar, gamma_opt])

    np.savetxt("results_%s/outs_ramiar_fix_H.txt"%FILENAME,output,delimiter=',',\
                        fmt='%10.5f',header='thk, GHF, T_S, Ramiar, gamma')
