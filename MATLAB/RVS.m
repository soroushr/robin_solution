function [temperature, depth] = RVS(M_dot, thk, H_flux, T_S, n_z)

%  Analytical solution from https://doi.org/10.1029/2018JF004774
%  that estimates the vertical temperature by approximating the
%  vertical velocity profile of SIA with a power function of 
%  form M(z/H)^gamma instead of M(z/H) which is used in the
%  Robin solution.
%
%  Args:
%       M_dot: surface vertical velocity
%              assumed to be the same as SMB [m/yr]
%       thk: thickness [m]
%       T_S: surface Dirichlet temperature [C]
%       gamma_: exponent of (z/H) in the new solution
%       H_flux: heat flux at the bottom [W/m2]
%       n_z: number of points on a vertical grid
% 
%  Returns:
%       temperature profile at depth [C]
%
%  USAGE:
%       [temperature, depth] = Robin(0.3, 3000, 0.05, -30, 5000);

    diffusivity = 34.4; % m2/yr
    conductivity = 2.10; % W/m/K

    depth = linspace(0, thk, n_z);

    % equation 19 in https://doi.org/10.1029/2018JF004774
    peclet = M_dot*thk/diffusivity;
    gamma_ = 1.390 + 0.044*log(peclet);

    % equation 18 https://doi.org/10.1029/2018JF004774
    lambda_ = M_dot / (diffusivity * thk^gamma_);
    phi = -lambda_/(gamma_+1.);

    cnst_subtractee = gammainc(-phi*thk^(gamma_+1.), 1./(1.+gamma_), 'upper') * gamma(1./(1.+gamma_));

    Y = zeros(length(depth), 1);
    for i = 1:length(depth)
        Y(i) = gammainc(-phi*depth(i).^(gamma_+1.), 1./(1.+gamma_), 'upper') * gamma(1./(1.+gamma_));
    end
    
    cnst_factor = H_flux/(conductivity*(gamma_+1.)) * (-phi)^( -1./(gamma_+1.) );

    T_temp = cnst_factor * (Y - cnst_subtractee);

    temperature = T_S + T_temp;

end
