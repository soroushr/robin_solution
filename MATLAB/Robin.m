function [temperature, depth] = Robin(M_dot, thk, H_flux, T_S, n_z)
 
%   Robin 1955 analytical solution, that approximates
%   the vertical velocity profile as M(z/H).
%   https://doi.org/10.3189/002214355793702028
%
%   Args:
%        M_dot: surface vertical velocity
%               assumed to be the same as SMB [m/yr]
%        thk: thickness [m]
%        H_flux: heat flux at the bottom [W/m2]
%        T_S: surface Dirichlet temperature [C]
%        n_z: number of points on a vertical grid
%
%   Returns:
%        temperature profile at depth [C]
%
%   USAGE:
%        [temperature, depth] = Robin(0.3, 3000, 0.05, -30, 5000);

    diffusivity = 34.4; % m2/yr
    conductivity = 2.10; % W/m/K

    depth = linspace(0, thk, n_z);
    q_sqrd = M_dot/2./(thk+eps)/diffusivity;
    q_term = sqrt(q_sqrd);
    
    temperature = T_S - H_flux*sqrt(pi)/(2.*conductivity*q_term) * (erf(depth*q_term)-erf(thk*q_term));

end
