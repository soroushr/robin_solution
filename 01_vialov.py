import constants
import os
import numpy as np
from dolfin import *
import matplotlib.pyplot as plt
from sympy.utilities.codegen import ccode
from sympy import symbols
import sympy as sp
from scipy.optimize import curve_fit
from util import (
    save_xml_and_pvd,
    save_xml,
    save_pvd
)

plt.rc('font', family='TeX Gyre Schola')

FILENAME = os.path.splitext(__file__)[0]
#################################
## some constants for glacier ice
lapse_rate     = constants.lapse_rate     # degrees per meter
conductivity   = constants.conductivity   # W/m/K
diffusivity    = constants.diffusivity    # m2/yr
surface_temp   = constants.surface_temp   # K
rho            = constants.rho            # kg/m3
gravity        = constants.gravity        # m/s2
rate_factor    = constants.rate_factor    # kPa-3 yr-1
beta           = constants.beta           # K/Pa
kelvin         = constants.kelvin         # K
eps            = constants.eps            # --
heat_capacity  = constants.heat_capacity  # J/kg/K
latent_fusion  = constants.latent_fusion  # kJ/kg
y2s            = constants.y2s
n_glen         = constants.n_glen         # exponent of Glen's flow law
M_dot          = constants.M_dot          # m/yr
Length         = constants.Length         # m
numElems       = constants.numElems
GHF            = constants.GHF            # W/m2
GAMMA          = constants.GAMMA

#---------------------------------
#       SYMBOLIC VIALOV
#---------------------------------

## symbolic declarations
x, y, vx, vy = symbols('x[0], x[1], vx, vy')

## vialov thickness constant (H0)
def vialov_constant():
    A0 = 2.*rate_factor/(n_glen+2.) \
       *(rho*gravity/1000.)**n_glen
    _coef = (2.*n_glen+2.)/(n_glen+1.)
    H0 = _coef*(M_dot/A0)**(1./n_glen)*Length**(1.+1./n_glen)

    return H0

def symbolic_thickness(H0, *args):
    H = (H0*(1.-(x/Length)**(1.+1./n_glen))) \
             **(n_glen/(2.*n_glen+2.))

    return H

def symbolic_slope(H, *args):
    dHdx   = sp.diff(H, x)

    return dHdx

def symbolic_tau_dx(H, *args):
    dHdx = symbolic_slope(H)
    tau_dx = -rho*gravity*H*dHdx/1000.

    return tau_dx

def symbolic_vx0(tau_dx, v_sliding=0, *args):
    vx0 = 2.*rate_factor*H/(n_glen+1.) \
           *tau_dx**n_glen + v_sliding

    return vx0

def symbolic_vz0(vx0, H, *args):
    dHdx = symbolic_slope(H)    
    vz0 = vx0*dHdx - M_dot

    return vz0
    
def mesh_to_geometry(mesh2d):
    """takes a 2d mesh and distorts all nodes
       to match that of the vialov profile
    """
    V2d = FunctionSpace(mesh2d, "CG", 1)
    nn  = V2d.dim()
    dd  = mesh2d.geometry().dim()

    dof_coordinates = V2d.dofmap().tabulate_all_coordinates(mesh2d)
    dof_coordinates.resize((nn, dd))
    dof_xx = dof_coordinates[:, 0]
    dof_yy = dof_coordinates[:, 1]

    x2d = mesh2d.coordinates()[:,0]
    y2d = mesh2d.coordinates()[:,1]

    # a function to project every x,y coordinate
    # of a rectangle mesh2d to the x,y coord of Vialov
    # set the minimum thickness to 5 to have a right
    # boundary for perscribing right BC. Otherwise fails
    def vialov_mesh(a, b):
        elevs   = [float(H.subs(x, i)) for i in a]
        elevs   = np.asarray(elevs)
        elevs [ elevs<10 ] = 5.
        return [a, b*elevs]

    x_bar, y_bar = vialov_mesh(x2d, y2d)
    xy_bar_coor = np.array([x_bar, y_bar]).transpose()

    return xy_bar_coor

def constant_at_depth(var, var_expression):
    # dofs for finding indices 
    dofmap  = V2d.dofmap()
    dof_x   = dofmap.tabulate_all_coordinates(mesh2d_vialov).reshape((V2d.dim(), -1))

    # coordinates of the old mesh used to find x-direction
    # intervals and corresponding indices
    coords = mesh1d.coordinates()

    # calculate driving stress from sym expression at any
    # given length discretization then apply the same 
    # value to the whole columns of the structured mesh
    for coord in coords:
        # this variable is just to set the "margin" for defining
        # the "near" at nodes - that division by 4 is arbitrary
        border = Length/float(numElems)/4.
        # 0 is columnar and 1 is along the bed
        # this is only possible because the mesh is structured
        xbed    = dof_x[:,0]
        indices = np.where(np.logical_and(xbed < coord+border, xbed > coord-border))[0]

        def _constant_at_depth(var_expression):
            """Projects a constant value that is calculated from
               lamellar flow at the surface to all the nodes at
               a given column of nodes. The returned function will
               have constant values at depth.
            """
            _local = var_expression.subs(x, coord)
            _list  = [_local*i for i in np.ones([N_DEPTH+1, 1])]
            _list  = np.asarray(_list)
            _list  = np.where(_list==sp.zoo, 400., _list)
            _list  = np.where(_list==sp.nan, 840., _list)
            return _list

        var.vector()[indices] = _constant_at_depth(var_expression)

    return var

## set the values at the ice divide the same as
## the second column of the mesh - problem is
## with how sympy evaluates gradients at the 
## end points
#idx_0 = np.where(np.logical_and(xbed < 0+border, xbed > 0-border))[0]
#idx_1 = np.where(np.logical_and(xbed < Length/float(numElems)+border, xbed > Length/float(numElems)-border))[0]

#for i, j in zip(idx_0, idx_1):
#    _ = w_surface.vector()[j]
#    w_surface.vector()[i] = float(_)

def calculate_u_depth(u_surface):

    V2d = FunctionSpace(mesh2d, "CG", 1)
    nn  = V2d.dim()
    dd  = mesh2d.geometry().dim()

    dof_coordinates = V2d.dofmap().tabulate_all_coordinates(mesh2d)
    dof_coordinates.resize((nn, dd))
    dof_xx = dof_coordinates[:, 0]
    dof_yy = dof_coordinates[:, 1]

    vx_temp = u_surface.vector()[:]*(1.-(1.-dof_yy)**(n_glen+1))

    V2d_vialov = FunctionSpace(mesh2d_vialov, "CG", 1)
    u_depth = Function(V2d_vialov)

    u_depth.vector()[:] = vx_temp

    return u_depth

def calculate_w_depth(u_surface, w_surface):

    V2d = FunctionSpace(mesh2d, "CG", 1)
    nn  = V2d.dim()
    dd  = mesh2d.geometry().dim()

    dof_coordinates = V2d.dofmap().tabulate_all_coordinates(mesh2d)
    dof_coordinates.resize((nn, dd))
    dof_xx = dof_coordinates[:, 0]
    dof_yy = dof_coordinates[:, 1]

    vx_temp = u_surface.vector()[:]*(1.-(1.-dof_yy)**(n_glen+1))
    u_depth.vector()[:] = vx_temp

    vz_temp = w_surface.vector()[:]*(1.-(1.-dof_yy)\
                    *((n_glen+2.)/(n_glen+1.)-(1.-dof_yy)**(n_glen+1.)/(n_glen+1.)))
    vz_temp = np.where(vz_temp>0, 0, vz_temp)

    V2d_vialov = FunctionSpace(mesh2d_vialov, "CG", 1)
    w_depth = Function(V2d_vialov)
    w_depth.vector()[:] = vz_temp

    return w_depth

def calculate_w_depth_Robin(mesh2d_vialov, M_dot_Robin, *args):
    V2d = FunctionSpace(mesh2d_vialov, "CG", 1)
    depth_exp = Expression("x[1]", degree=1, domain=mesh2d_vialov)
    depth = project(depth_exp, V2d)
    _w_depth_Robin = -abs(M_dot_Robin)*depth/thickness
    w_depth_Robin  = project(_w_depth_Robin, V2d)

    return w_depth_Robin

def calculate_w_depth_SRB(mesh2d_vialov, M_dot_Robin, _gamma, *args):
    V2d = FunctionSpace(mesh2d_vialov, "CG", 1)
    depth_exp = Expression("x[1]", degree=1, domain=mesh2d_vialov)
    depth = project(depth_exp, V2d)
    _w_depth_SRB = -abs(M_dot_Robin)*(depth/(thickness+DOLFIN_EPS))**_gamma
    w_depth_SRB  = project(_w_depth_SRB, V2d)

    return w_depth_SRB

def calculate_w_depth_compressibility(mesh2d_vialov, vx, *args):

    V2d = FunctionSpace(mesh2d_vialov, "CG", 2)
    # variational formulation
    u   = TrialFunction(V2d)
    psi = TestFunction(V2d)

    # no w at the bottom of the domain
    def boundary_bottom(x, on_boundary):
        return on_boundary and near(x[1], 0, DOLFIN_EPS)

    bc_bottom = DirichletBC(V2d, Constant(0.), boundary_bottom)
    bc = [bc_bottom]

    # divergence terms
    # d/dx(vx) + d/dy(w) = 0
    # d/dx(vx) is from expression
    f = vx.dx(0)

    a = u.dx(1)*psi*dx
    L = -f*psi*dx

    # solve
    w = Function(V2d)

    solve(a == L, w, bc)

    return w

def get_depth(mesh):
    depth_exp = Expression("x[1]", degree=1, domain=mesh)
    
    return project(depth_exp, V2d)

def plot_w(w):
    dofmap  = V2d.dofmap()
    dof_x   = dofmap.tabulate_all_coordinates(mesh2d_vialov).reshape((V2d.dim(), -1))

    # coordinates of the old mesh used to find x-direction
    # intervals and corresponding indices
    coords = mesh1d.coordinates()

    depth = get_depth(mesh2d_vialov)
    
    # calculate driving stress from sym expression at any
    # given length discretization then apply the same 
    # value to the whole columns of the structured mesh
    for coord in coords:
        if coord%50000==0 and coord<Length and coord>0:
            # this variable is just to set the "margin" for defining
            # the "near" at nodes - that division by 4 is arbitrary
            border = Length/float(numElems)/4.
            # 0 is columnar and 1 is along the bed
            # this is only possible because the mesh is structured
            xbed    = dof_x[:,0]
            indices = np.where(np.logical_and(xbed < coord+border, xbed > coord-border))[0]
            plt.plot(w.vector()[indices], depth.vector()[indices], 'go-', lw=1, alpha=0.7, label='$v_z$ SIA')
            minn = min(w.vector()[indices])
            maxx = max(w.vector()[indices])
            _len = len(indices)
            _thk = max(thickness.vector()[indices])
            plt.plot(np.linspace(minn,maxx,_len), depth.vector()[indices], 'k--', lw=2, alpha=0.7, label='$v_z$ Robin')

#            def func(x, a, b):
#                return minn*a*(x/_thk)**b

#            popt, pcov = curve_fit(func, depth.vector()[indices], \
#                                        w.vector()[indices], maxfev=1000000)
#            plt.plot(func(depth.vector()[indices], *popt), depth.vector()[indices],\
#                        'r-', label=r'$v_z=$%2.3f * %.2f * $ (\frac {z}{%i})^{%5.3f}$' % (minn, popt[0], int(_thk), popt[1]) )

            # this forms works OK but not great
            def func(x, a):
                return minn*(x/_thk)**a

            popt, pcov = curve_fit(func, depth.vector()[indices], \
                                        w.vector()[indices], maxfev=1000000)
            plt.plot(func(depth.vector()[indices], *popt), depth.vector()[indices],\
                        'r-', alpha=0.7, label=r'$v_z=$%2.3f * $(\frac {z}{%i})^{%5.3f}$' % (minn, int(_thk), popt[0]) )

            plt.gca().set_ylim(bottom=0)
            plt.grid(linestyle='dotted')
            plt.title( '$v_z$ at %3.1f km from the divide'%(coord/1000.) )
            plt.xlabel('vertical velocity [m/yr]')
            plt.ylabel('depth [m]')
            plt.legend(loc=1)
            plt.savefig("results_%s/figs/%s.png"%(FILENAME,str(coord)), dpi=200)
            print "vz at coordinate %i saved"%coord
            plt.clf()
            _x = w.vector()[indices]
            _y = depth.vector()[indices]
            _lst = np.asarray([_x, _y])
            np.savetxt("results_%s/figs/%s.txt"%(FILENAME,str(coord)), _lst.T, \
                        delimiter=',', fmt='%10.5f')


if __name__ == "__main__":
    #---------------------------------
    #       CALCULATE VIALOV
    #---------------------------------
    H0     = vialov_constant()

    H      = symbolic_thickness(H0=H0)
    slope  = symbolic_slope(H=H)
    tau_dx = symbolic_tau_dx(H=H)
    vx0    = symbolic_vx0(tau_dx=tau_dx)
    vz0    = symbolic_vz0(vx0=vx0, H=H)

    #--------------------------------------------------
    #  MESH (1D AND 2D) FROM VIALOV & FUNCTIONSPACES
    #--------------------------------------------------
    # 1d mesh along the ice sheet
    mesh1d = IntervalMesh(numElems, 0.0, Length)

    # 2d "shallow" mesh
    # x -> [0, Length], y -> [0, 1]
    N_ALONG = numElems
    N_DEPTH = 20

    mesh2d        = RectangleMesh(Point(0., 0.), Point(Length, 1), N_ALONG, N_DEPTH, "right")
    mesh2d_vialov = RectangleMesh(Point(0., 0.), Point(Length, 1), N_ALONG, N_DEPTH, "right")

    # new mesh and the new corresponding functionspace
    mesh2d_vialov.coordinates()[:] = mesh_to_geometry(mesh2d)
    V2d = FunctionSpace(mesh2d_vialov, "CG", 1)

    #---------------------------------
    #     VARIABLES AS FUNCTIONS
    #---------------------------------
    depth         = Function(V2d)
    tau           = Function(V2d)
    u_surface     = Function(V2d)
    w_surface     = Function(V2d)
    thickness     = Function(V2d)
    u_depth       = Function(V2d)
    w_depth       = Function(V2d)
    w_depth_Robin = Function(V2d)
    w_depth_SRB   = Function(V2d)

    tau       = constant_at_depth(tau, tau_dx)
    thickness = constant_at_depth(thickness, H)
    u_surface = constant_at_depth(u_surface, vx0)
    w_surface = constant_at_depth(w_surface, vz0)

    # three vertical velocities - one from SIA
    # one Mz/H with uniform M as acc rate
    # oen Mz/H with M as surface vz from SIA
    u_depth                 = calculate_u_depth(u_surface)
    w_depth                 = calculate_w_depth(u_surface, w_surface)
    w_depth_compressibility = calculate_w_depth_compressibility(mesh2d_vialov, u_depth)
    w_depth_Robin_M_dot     = calculate_w_depth_Robin(mesh2d_vialov, M_dot_Robin = M_dot)
    w_depth_Robin_w_surface = calculate_w_depth_Robin(mesh2d_vialov, M_dot_Robin = w_surface)
    w_depth_SRB_M_dot     = calculate_w_depth_SRB(mesh2d_vialov, M_dot_Robin = M_dot, _gamma = GAMMA)
    w_depth_SRB_w_surface = calculate_w_depth_SRB(mesh2d_vialov, M_dot_Robin = w_surface, _gamma = GAMMA)

    depth = get_depth(mesh2d_vialov)
    #---------------------------------
    #     SAVE MESH AND VARIABLES
    #---------------------------------
    save_xml('mesh2d_vialov', mesh2d_vialov, FILENAME)

    save_xml_and_pvd('depth'                  , depth                  , FILENAME)
    save_xml_and_pvd('thickness'              , thickness              , FILENAME)
    save_xml_and_pvd('tau'                    , tau                    , FILENAME)
    save_xml_and_pvd('w_surface'              , w_surface              , FILENAME)
    save_xml_and_pvd('u_surface'              , u_surface              , FILENAME)
    save_xml_and_pvd('u_depth'                , u_depth                , FILENAME)
    save_xml_and_pvd('w_depth'                , w_depth                , FILENAME)
    save_xml_and_pvd('w_depth_compressibility', w_depth_compressibility, FILENAME)
    save_xml_and_pvd('w_depth_Robin_M_dot'    , w_depth_Robin_M_dot    , FILENAME)
    save_xml_and_pvd('w_depth_Robin_w_surface', w_depth_Robin_w_surface, FILENAME)
    save_xml_and_pvd('w_depth_SRB_M_dot'      , w_depth_SRB_M_dot    , FILENAME)
    save_xml_and_pvd('w_depth_SRB_w_surface'  , w_depth_SRB_w_surface, FILENAME)

    #---------------------------------
    #   PLOT VERTICAL VELOCITIES
    #---------------------------------
#    plot_w(w_depth)

#-----------------------------------------------------
#     STRAIN HEATING AS A FUNCTION OF DISTANCE
#-----------------------------------------------------
SAVE_VEL = True
if SAVE_VEL:
    dofmap = V2d.dofmap()
    dof_x = dofmap.tabulate_all_coordinates(mesh2d).reshape((V2d.dim(), -1))
    xx = dof_x[:, 0]
    yy = dof_x[:, 1]
    
    # get basal indices
    indices = np.where(np.logical_and(yy<0.001, yy<0.001))[0]
    vel_srf_vals = u_surface.vector()[indices]

    # Get coordinates of dof
    xcoord = dof_x[indices]

    x_axis = xcoord[:,0]/float(Length)
    print x_axis.shape
    '''
    kwargs = {"color":'k', "lw":3., "alpha":1., "label":'strain heating'}
    plt.plot(xcoord/float(Length), strain_vals, **kwargs)

    kwargs = {"color":'grey', "ls":"--","lw":1., "alpha":0.5}
    plt.plot(xcoord/float(Length), np.ones(len(xcoord))*30, **kwargs)
    kwargs = {"color":'grey', "ls":"--","lw":1.5, "alpha":1, "label":'GHF=50 mW m$^{-2}$'}
    plt.plot(xcoord/float(Length), np.ones(len(xcoord))*50, **kwargs)
    kwargs = {"color":'grey', "ls":"--","lw":1., "alpha":0.5}
    plt.plot(xcoord/float(Length), np.ones(len(xcoord))*70, **kwargs)

    plt.gca().axhspan(30, 70, alpha=0.2, color='grey')

    plt.grid(linestyle='dotted')
    plt.xlim([0,.8])
    plt.ylim([0,300])
    plt.gca().set_ylim(bottom=0)
#    plt.legend(bbox_to_anchor=(1.01, 0.7), loc='upper left', borderaxespad=0., prop={'size': 8})
    plt.xlabel(r'relative distance from the ice divide $x/L$')
    plt.ylabel(r'strain heating [mW m$^{-2}$]')
    plt.legend(loc=2)
#    plt.legend(loc=3, ncol=3, prop={'size': 8})
    plt.savefig("results_%s/strain_heating_Mdot_%.1f.png"%(FILENAME,M_dot),dpi=300)
    '''
    stacked = np.vstack([x_axis.T, vel_srf_vals])
    np.savetxt('results_%s/srf_vels.txt'%(FILENAME), stacked.T,\
                header='x, vx_surface', fmt='%.5f')
