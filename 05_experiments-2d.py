import constants
from mpmath import gammainc, mpf
import os
import numpy as np
from dolfin import *
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy.interpolate import spline
from util import (
    save_xml_and_pvd,
    save_xml,
    save_pvd
)

plt.rc('font', family='TeX Gyre Schola')

FILENAME = os.path.splitext(__file__)[0]
IN_DIR   = './results_01_vialov/'
#################################
## some constants for glacier ice
lapse_rate     = constants.lapse_rate     # degrees per meter
conductivity   = constants.conductivity   # W/m/K
diffusivity    = constants.diffusivity    # m2/yr
surface_temp   = constants.surface_temp   # K
rho            = constants.rho            # kg/m3
gravity        = constants.gravity        # m/s2
rate_factor    = constants.rate_factor    # kPa-3 yr-1
beta           = constants.beta           # K/Pa
kelvin         = constants.kelvin         # K
eps            = constants.eps            # --
heat_capacity  = constants.heat_capacity  # J/kg/K
latent_fusion  = constants.latent_fusion  # kJ/kg
y2s            = constants.y2s
n_glen         = constants.n_glen         # exponent of Glen's flow law
M_dot          = constants.M_dot          # m/yr
Length         = constants.Length         # m
numElems       = constants.numElems
GHF            = constants.GHF            # W/m2
GAMMA          = constants.GAMMA
GAMMA_OPTIMAL  = constants.GAMMA_OPTIMAL
COEFF_FIT      = constants.COEFF_FIT

#-----------------------------------
#       LOAD VIALOV VARIABLES
#-----------------------------------
mesh2d = Mesh(IN_DIR+'mesh2d_vialov.xml')
V2d = FunctionSpace(mesh2d, "CG", 1)

depth                   = Function(V2d, IN_DIR+'depth.xml')
u_depth                 = Function(V2d, IN_DIR+'u_depth.xml')
u_surface               = Function(V2d, IN_DIR+'u_surface.xml')
w_surface               = Function(V2d, IN_DIR+'w_surface.xml')
w_depth                 = Function(V2d, IN_DIR+'w_depth.xml')
w_depth_Robin_M_dot     = Function(V2d, IN_DIR+'w_depth_Robin_M_dot.xml')
w_depth_Robin_w_surface = Function(V2d, IN_DIR+'w_depth_Robin_w_surface.xml')
w_depth_SRB_M_dot       = Function(V2d, IN_DIR+'w_depth_SRB_M_dot.xml')
w_depth_SRB_w_surface   = Function(V2d, IN_DIR+'w_depth_SRB_w_surface.xml')
thickness               = Function(V2d, IN_DIR+'thickness.xml')
tau                     = Function(V2d, IN_DIR+'tau.xml')

peclet    = project(abs(w_surface*thickness)/diffusivity, V2d)
opt_gamma = 1.390 + 0.044*ln(peclet)
#---------------------------
#       MARK BOUNDARIES
#---------------------------
boundary_parts = FacetFunction('size_t', mesh2d)
boundary_parts.set_all(0)
for f in facets(mesh2d):
  if any(ff.exterior() for ff in facets(f)):
    boundary_parts[f] = 3

bottom = AutoSubDomain(lambda x: near(x[1], 0.0))
left   = AutoSubDomain(lambda x: near(x[0], 0.0))
right  = AutoSubDomain(lambda x: near(x[0], Length))

left  .mark(boundary_parts, 1)
bottom.mark(boundary_parts, 2)
right .mark(boundary_parts, 4)

#---------------------------
#      INSERT VELOCITY
#---------------------------
## assemble x and z velocities as vector
## three options for vz for comparison
velocity = as_vector([u_depth, w_depth])
#velocity = as_vector([u_depth, w_depth_Robin_M_dot])
#velocity = as_vector([u_depth, w_depth_Robin_w_surface])
#velocity = as_vector([0, w_depth_Robin_w_surface])
#velocity = as_vector([0, w_depth_SRB_w_surface])

#------------------------------
#    ADV-DIFF FEM SOLUTION
#------------------------------
ANISOTROPIC = False
def solve_variational_form(vel=velocity, T_constant=surface_temp, lrate=lapse_rate,
                            diff_only=False, *args):
    """Args:
        - diff_only: if True means that only the differences
          between surface and basal temperature is calculated
    """
    # assign boundary values
    bottom_gradient = GHF * (diffusivity/conductivity)

    src        = Constant(0.)                            # no source term
    g_bottom   = Constant(bottom_gradient)               # Neumann heat flux
    g_left     = Constant(0.)                            # Neumann heat flux
    u0_top     = Expression("A - x[1]*B", A=T_constant, B=lrate, \
                            degree=1, domain=mesh2d)     # Dirichlet surface
    u0_right   = Constant(surface_temp)                  # Dirichlet terminus
    dsN        = Measure("ds", subdomain_id=1, subdomain_data=boundary_parts)
    dsN        = Measure("ds", subdomain_id=4, subdomain_data=boundary_parts)
    dsN        = Measure("ds", subdomain_id=2, subdomain_data=boundary_parts)

    n_vector   = FacetNormal(mesh2d)
    # Define boundary condition
    bc_top   = DirichletBC(V2d, u0_top  , boundary_parts, 3)
    bc_right = DirichletBC(V2d, u0_right, boundary_parts, 4)
    bcs = [bc_top, bc_right]

    # Test and trial functions
    u   = TrialFunction(V2d)
    psi = TestFunction(V2d)

    # Galerkin variational problem
    # Add SUPG stabilisation terms
    vnorm = sqrt( dot(vel, vel) )
    h = CellSize(mesh2d)

    psihat = psi + h/(2*vnorm) + dot(vel, grad(psi))

    if ANISOTROPIC:
        kappa = as_matrix([[diffusivity, 0],
                           [0, diffusivity]])
    else:
        kappa = diffusivity

    # advection-diffusion problem
    F = + psihat * dot(vel, grad(u)) * dx \
        + dot(grad(psi), kappa*grad(u)) * dx \
        - src * psihat * dx \
        - g_bottom * psihat * dsN(2) \
        - g_left * psihat * dsN(1) \
        #- g_left * dot(grad(psihat), n_vector) * dsN(1) \
        #- g_right * psihat * dsN(4)

    # Create bilinear and linear forms
    a = lhs(F)
    L = rhs(F)

    # Solve
    temperature = Function(V2d)
    solve(a == L, temperature, bcs)

    if not diff_only:
        return temperature
    else:
        return project(temperature-expression_at_depth(mesh2d, u0_top), V2d)

#----------------------------------
#    Robin TEMPERATURE SOLUTION    
#----------------------------------
def expression_at_depth(mesh2d, expression, *args):
    Ts_Robin = Function(V2d)
    # because the expression for surface temperature
    # is essentially only a function of z, the expression
    # is applied to all nodes with the z from the thickness
    # function which is constant at depth
    # FIXME: there must be a better way!  
    nvertices = mesh2d.ufl_cell().num_vertices()

    # Set up a vertex_2_dof list
    dofmap  = V2d.dofmap()
    indices = [dofmap.tabulate_entity_dofs(0, i)[0] for i in range(nvertices)]

    vertex_2_dof = dict()
    [vertex_2_dof.update(dict(vd for vd in zip(cell.entities(0), \
                            dofmap.cell_dofs(cell.index())[indices]))) \
                            for cell in cells(mesh2d)]

    # Get the vertex coordinates
    coords = mesh2d.coordinates()

    # Set the vertex coordinate you want to modify
    for xcoord, ycoord in coords:
        vertex_idx = np.where((coords == (xcoord,ycoord)).all(axis = 1))[0] 

        vertex_idx = vertex_idx[0]
        dof_idx = vertex_2_dof[vertex_idx]
        Ts_Robin.vector()[dof_idx] = expression(xcoord, thickness(xcoord, ycoord))

    return Ts_Robin

# Robin EQUATION
def Robin_solution(M_dot_Robin, T_constant=surface_temp, lrate=lapse_rate,
                   diff_only=False, *args):
    """Args:
        - diff_only: if True means that only the differences
          between surface and basal temperature is calculated
    """
    u0_top     = Expression("A - x[1]*B", A=T_constant, B=lrate, \
                        degree=1, domain=mesh2d)     # Dirichlet surface
    T_depth  = Function(V2d)
    Ts_Robin = expression_at_depth(mesh2d, u0_top)
    T_temp   = Function(V2d)

    q_sqrd = abs(M_dot_Robin/2./(thickness+DOLFIN_EPS)/diffusivity)
    q_term = sqrt(q_sqrd)

    T_temp = - GHF*sqrt(pi)/(2. * conductivity * q_term) * \
                ( erf(depth * q_term) - erf(thickness * q_term) )
    T_depth = Ts_Robin + T_temp

    if not diff_only:
        return project(T_depth, V2d)
    else:
        return project(T_temp, V2d)

#----------------------------------
#    SRB TEMPERATURE SOLUTION    
#----------------------------------
def SRB_solution(M_dot_SRB, T_constant=surface_temp, lrate=lapse_rate,
                 diff_only=False, *args):
    """Args:
        - diff_only: if True means that only the differences
          between surface and basal temperature is calculated
    """
    u0_top   = Expression("A - x[1]*B", A=T_constant, B=lrate, \
                        degree=1, domain=mesh2d)     # Dirichlet surface
    T_depth  = Function(V2d)
    Ts_SRB   = expression_at_depth(mesh2d, u0_top)
    T_temp   = Function(V2d)
    
    opt_gamma_float = project(opt_gamma, V2d)

    coords = mesh2d.coordinates()
    _vertex_to_dof_map = vertex_to_dof_map(V2d)

    for i in range(V2d.dim()):
        vertex_index = _vertex_to_dof_map[i]

        x, y = coords[i]
        thk  = abs(thickness(x,y) + DOLFIN_EPS)
        dpth = abs(depth(x,y) + DOLFIN_EPS)
        _gamma = float(opt_gamma_float(x,y))-0.01

        if isinstance(M_dot_SRB, float):
            acc = abs(M_dot_SRB)
        else:
            acc = abs(M_dot_SRB(x,y) + DOLFIN_EPS)

        lmbd = acc / (diffusivity * thk**_gamma + DOLFIN_EPS) + DOLFIN_EPS
        zeta = -lmbd/(_gamma+1.) + DOLFIN_EPS

        cnst_factor = GHF/(conductivity*(_gamma+1.))

        cnst_H = -zeta*thk**(_gamma+1.)
        cnst_subtractee = thk*(-zeta*thk**(_gamma+1.))**(-1./(_gamma+1.)) *\
                        gammainc(1./(1.+_gamma), cnst_H )

        cnst_z = -zeta*dpth**(_gamma+1.)
        _lst = cnst_factor *\
                (dpth*(-zeta*dpth**(_gamma+1.))**(-1./(_gamma+1.)) *\
                gammainc(1./(1.+_gamma), cnst_z ) - \
                cnst_subtractee)

        lst = np.array([_lst], dtype=float)        

        T_temp.vector()[vertex_index] = lst[0]

    T_depth = Ts_SRB + T_temp

    if not diff_only:
        return project(T_depth, V2d)
    else:
        return project(T_temp, V2d)


#----------------------------------
#       PLOT TEMPERATURES
#----------------------------------

# function to plot 2d variables on the mesh
def plot_2d(fnc, lowlim, uplim, title, 
            plot_args={}, colorbar_args = {}, **kwargs):

    v = fnc.compute_vertex_values(mesh2d)
    x = mesh2d.coordinates()[:,0]/1000. # to km
    y = mesh2d.coordinates()[:,1]
    t = mesh2d.cells()

#    plt.tricontour(x, y, v, 1, linewidths=0.05, colors='k')
#    cm = plt.get_cmap('PRGn', 40)
    cm = plt.get_cmap('seismic', 40)

    clim=(lowlim, uplim)
    sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=clim[0], vmax=clim[1]))
    sm._A = []
    cbar = plt.colorbar(sm, orientation='horizontal')

    cs = plt.tricontourf(x, y, t, v, 100, vmin = lowlim, vmax = uplim, cmap=cm)

    labels = np.linspace(clim[0], clim[1], 11)
    cbar.set_ticks(labels)
    cbar.set_ticklabels(labels)
    plt.xlim([x.min(), x.max()]);
    plt.ylim([y.min(), y.max()])
    plt.axes().set_aspect(0.1)
    plt.xlabel('distance from the ice divide [km]')
    plt.ylabel('z [m]')
    plt.title(title)
    print '%s plotted'%title

# function to plot variable at the bed along the flow
def plot_basal_along_flow(fnc, plot_args={}, **kwargs):
    dofmap = V2d.dofmap()
    dof_x = dofmap.tabulate_all_coordinates(mesh2d).reshape((V2d.dim(), -1))
    xx = dof_x[:, 0]
    yy = dof_x[:, 1]

    indices = np.where(np.logical_and(yy<0.001, yy<0.001))[0]
    # Get coordinates of dof
    xcoord = dof_x[indices]
    # Get value of dof 
    vals = fnc.vector()[indices]

#    plt.gcf().add_axes([0.1, 0.1, 0.70, 0.80])
    plt.plot(xcoord[:-2]/1000., vals[:-2], **plot_args)
    plt.grid(linestyle='dotted')
#    plt.xlim([0,Length/1000.+5.])
#    plt.gca().set_ylim(bottom=0)
#    plt.legend(bbox_to_anchor=(1.01, 0.7), loc='upper left', borderaxespad=0., prop={'size': 8})
    plt.xlabel('distance from the ice divide [km]')
    plt.ylabel(r'$T_b - T_s$ [$^{\circ}$C]')
#    plt.legend(loc=3)
    plt.legend(loc=3, ncol=3, prop={'size': 8})
    plt.xlim([0,600])
    plt.ylim([5, 30])

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#----------------------------------
#     TEMPERATURE COMPARISON
#----------------------------------
SAVE_2D_PLOTS = False
if SAVE_2D_PLOTS:
    #temperature_Robin = Robin_solution(M_dot_Robin = M_dot)
    #temperature_SRB   = SRB_solution(M_dot_SRB=M_dot)

    temperature_Robin = Robin_solution(M_dot_Robin=w_surface)
    temperature_SRB   = SRB_solution(M_dot_SRB=w_surface)

    temperature_full  = solve_variational_form(lrate=lapse_rate)

    difference_SRB    = temperature_SRB - temperature_full
    difference_Robin  = temperature_Robin - temperature_full

    plot_2d(fnc=project(difference_Robin, V2d), lowlim=-10, uplim=10, \
            title=r'Robin solution - full solution [$^{\circ}$C]' +\
                  r' with lapse rate=%.1f [$^{\circ}$C/km]'%(lapse_rate*1000.))
    plt.savefig("results_%s/figs/lrate_%.4f_diff_robin.png"%(FILENAME,lapse_rate), dpi=300)
    plt.clf()

    plot_2d(fnc=project(difference_SRB, V2d), lowlim=-10, uplim=10, \
            title=r'analytical - numerical solution [$^{\circ}$C]' +\
                  r' with lapse rate=%.1f [$^{\circ}$C km$^{-1}$]' % (lapse_rate*1000.))
    plt.savefig("results_%s/figs/lrate_%.4f_diff_srb.png"%(FILENAME,lapse_rate), dpi=300)
    plt.clf()

    save_xml_and_pvd('temperature_full'      , temperature_full              , FILENAME)
    save_xml_and_pvd("temperature_Robin"     , temperature_Robin             , FILENAME)
    save_xml_and_pvd("temperature_SRB"       , temperature_SRB               , FILENAME)
    save_xml_and_pvd("temperature_diff_SRB"  , project(difference_SRB,   V2d), FILENAME)
    save_xml_and_pvd("temperature_diff_Robin", project(difference_Robin, V2d), FILENAME)

#-----------------------------------------------------------
#     APPLICABILITY DISTANCE AS A FUNCTION OF LAPSE RATE
#-----------------------------------------------------------
## solution with optimal gamma
DISTANCE_APP = True
if DISTANCE_APP:
    temperature_SRB = SRB_solution(M_dot_SRB=w_surface, diff_only=True)

    ## full solution with lapse rate range
    _lapse_rates = np.linspace(0.0025, 0.0090, 30)
    THRESHOLDS   = [0.5, 1, 2]
    cmap = mpl.cm.copper_r
    counter = 0
    for THRESHOLD in THRESHOLDS:
        dists_plt = []
        for item in _lapse_rates:
            print counter, THRESHOLD, item
            counter += 1
            temperature_full  = solve_variational_form(lrate=item, diff_only=True)

            dofmap = V2d.dofmap()
            dof_x = dofmap.tabulate_all_coordinates(mesh2d).reshape((V2d.dim(), -1))
            xx = dof_x[:, 0]
            yy = dof_x[:, 1]

            # get basal indices
            indices = np.where(np.logical_and(yy<0.001, yy<0.001))[0]

            # Get coordinates of dof
            xcoord = dof_x[indices]

            # Get basal values and basal indices
            Tb_full = temperature_full.vector()[indices]
            Tb_SRB  = temperature_SRB.vector()[indices]

            Tb_diff = Tb_full - Tb_SRB
            _index = next((i for i, v in enumerate(Tb_diff) if abs(v) > THRESHOLD), None)

            _location = xcoord[_index][0]
            print 'location type is', type(_location)
            if isinstance(_location ,np.ndarray):
                _location = 0.999*Length
            dists_plt.append(_location)

        clr = cmap( (THRESHOLD-THRESHOLDS[0])/(THRESHOLDS[-1]-THRESHOLDS[0]) )
        kwargs = {"color":clr, "lw":2., "alpha":1., "label":r'%.1f$^{\circ}$C difference'%THRESHOLD}

        xplt = _lapse_rates*1000.
        yplt = np.asarray(dists_plt)/float(Length)
        yplt = np.ma.masked_where(np.isnan(yplt), yplt)
        print yplt[0], type(yplt), yplt.shape
        print xplt[0], type(xplt), xplt.shape
        plt.plot(xplt, yplt, **kwargs)
        plt.legend(loc=2)
        plt.grid(linestyle='dotted')
        np.savetxt("results_%s/threshold_%s.txt"%(FILENAME,str(THRESHOLD)), np.vstack([xplt,yplt]).T, delimiter=',',\
                    header='LR, dist', fmt='%.5f')

    plt.xlabel(r'lapse rate [$^{\circ}$C km$^{-1}$]')
    plt.ylabel(r'relative distance from the ice divide $x/L$')
    plt.savefig("results_%s/thresholds.png"%FILENAME,dpi=300)

#-----------------------------------------------------
#     STRAIN HEATING AS A FUNCTION OF DISTANCE
#-----------------------------------------------------
STRAIN_HEATING = False
if STRAIN_HEATING:
    G_strain = 2./5.*rate_factor*thickness*(tau**4.)/y2s*1e6 # in mW/m2

    dofmap = V2d.dofmap()
    dof_x = dofmap.tabulate_all_coordinates(mesh2d).reshape((V2d.dim(), -1))
    xx = dof_x[:, 0]
    yy = dof_x[:, 1]
    
    # get basal indices
    indices = np.where(np.logical_and(yy<0.001, yy<0.001))[0]
    vals = project(G_strain, V2d).vector()[indices]

    # Get coordinates of dof
    xcoord = dof_x[indices]

    kwargs = {"color":'k', "lw":3., "alpha":1., "label":'strain heating'}
    plt.plot(xcoord/float(Length), vals, **kwargs)

    kwargs = {"color":'grey', "ls":"--","lw":1., "alpha":0.5}
    plt.plot(xcoord/float(Length), np.ones(len(xcoord))*30, **kwargs)
    kwargs = {"color":'grey', "ls":"--","lw":1.5, "alpha":1, "label":'GHF=50 mW/m$^2$'}
    plt.plot(xcoord/float(Length), np.ones(len(xcoord))*50, **kwargs)
    kwargs = {"color":'grey', "ls":"--","lw":1., "alpha":0.5}
    plt.plot(xcoord/float(Length), np.ones(len(xcoord))*70, **kwargs)

#    plt.fill_between(xcoord/float(Length),np.ones(len(xcoord))*30,\
#                     np.ones(len(xcoord))*70,color='grey',alpha=.2)
    plt.gca().axhspan(30, 70, alpha=0.2, color='grey')

    plt.grid(linestyle='dotted')
    plt.xlim([0,.8])
    plt.ylim([0,300])
    plt.gca().set_ylim(bottom=0)
#    plt.legend(bbox_to_anchor=(1.01, 0.7), loc='upper left', borderaxespad=0., prop={'size': 8})
    plt.xlabel(r'relative distance from the ice divide $x/L$')
    plt.ylabel(r'strain heating [mW m$^{-2}$]')
    plt.legend(loc=2)
#    plt.legend(loc=3, ncol=3, prop={'size': 8})
    plt.savefig("results_%s/strain_heating.png"%FILENAME,dpi=300)
    np.savetxt('results_%s/vals.txt'%FILENAME, vals, fmt='%.5f')
