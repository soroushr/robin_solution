import matplotlib.pyplot as plt
import matplotlib as mpl
import os, sys
from dolfin import *
import numpy as np
import constants
from util import (
    solve_variational_form,
    SRB_solution,
    Robin_solution
)

plt.rc('font', family='TeX Gyre Schola')

FILENAME = os.path.splitext(__file__)[0]
#################################
## some constants for glacier ice
M_dot         = constants.M_dot
GAMMA         = constants.GAMMA
GAMMA_OPTIMAL = constants.GAMMA_OPTIMAL

THICKNESS = 3000.
INTERVALS = 200
T_SURFACE = -30

#-----------------------------------
#   FENICS MESH AND FUNCTIONSPACE
#-----------------------------------
mesh = IntervalMesh(INTERVALS, 0, THICKNESS)
Q    = FunctionSpace(mesh, "CG", 1)

#------------------------------------------------
# define velocity and solve T for Robin solution,
# our solution, and FEniCS numerical solution
#------------------------------------------------
# the expression for vz_SIA_exact is from
# https://www.the-cryosphere-discuss.net/tc-2017-276/tc-2017-276.pdf
vz_1st         = Expression(("-A*pow(x[0],1)/pow(B,1)"), A=M_dot, B=THICKNESS, degree=1)
vz_SIA_fit     = Expression(("-A*pow(x[0],C)/pow(B,C)"), A=M_dot, B=THICKNESS, C=GAMMA, degree=2)
vz_SIA_optimal = Expression(("-A*pow(x[0],C)/pow(B,C)"), A=M_dot, B=THICKNESS, C=GAMMA_OPTIMAL, degree=2)
vz_SIA_exact   = Expression(("-A/4*(pow(1-x[0]/B,5)+5*x[0]/B-1)"), A=M_dot, B=THICKNESS, degree=5)

# additional Vz's with different Glen's exponents
vz_SIA_exact_4  = Expression(("-A/(C+1)*(pow(1-x[0]/B,(C+2))+(C+2)*x[0]/B-1)"), A=M_dot, B=THICKNESS, C=4, degree=6)
vz_SIA_exact_1  = Expression(("-A/(C+1)*(pow(1-x[0]/B,(C+2))+(C+2)*x[0]/B-1)"), A=M_dot, B=THICKNESS, C=1, degree=6)

temperature_1st          = solve_variational_form(vz_1st, mesh=mesh, Q=Q)
temperature_SIA_fit      = solve_variational_form(vz_SIA_fit, mesh=mesh, Q=Q)
temperature_SIA_optimal  = solve_variational_form(vz_SIA_optimal, mesh=mesh, Q=Q)
temperature_SIA_exact    = solve_variational_form(vz_SIA_exact, mesh=mesh, Q=Q)
temperature_SIA_exact_4  = solve_variational_form(vz_SIA_exact_4, mesh=mesh, Q=Q)
temperature_SIA_exact_1 = solve_variational_form(vz_SIA_exact_1, mesh=mesh, Q=Q)

temperature_robin = Robin_solution(M_dot_robin=M_dot, mesh=mesh, Q=Q, thk=THICKNESS)
temperature_srb   = SRB_solution(M_dot_srb=M_dot, mesh=mesh, Q=Q, thk=THICKNESS)

#-----------------------------------
#              PLOTS
#-----------------------------------
temperature_1st          = temperature_1st         .vector().array()[::-1]
temperature_SIA_fit      = temperature_SIA_fit     .vector().array()[::-1]
temperature_SIA_optimal  = temperature_SIA_optimal .vector().array()[::-1]
temperature_SIA_exact    = temperature_SIA_exact   .vector().array()[::-1]
temperature_SIA_exact_4  = temperature_SIA_exact_4 .vector().array()[::-1]
temperature_SIA_exact_1 = temperature_SIA_exact_1.vector().array()[::-1]

temperature_robin = temperature_robin.vector().array()[::-1]
temperature_srb   = temperature_srb  .vector().array()[::-1]

coord = mesh.coordinates()[:,0]

##############################
#       PLOT PROBLEM
##############################
plt.plot(temperature_robin      , coord, 'red', ls=':' , lw=2, alpha=1,) \
#            label=r'Robin solution $v_z=-\dot M\frac{z} {H}$')
#plt.plot(temperature_SIA_fit    , coord, 'g--'  , lw=3, alpha=0.7,) \
#            label=r'Our numerical solution $v_z=-\dot M(\frac{z} {H})^\gamma, \gamma_{SIA}$=%.3f'%(GAMMA))
#plt.plot(temperature_SIA_optimal, coord, 'g', ls='-'   , lw=2, alpha=0.7, \
#            label=r'Our solution $v_z=-\dot M(\frac{z} {H})^\gamma, \gamma_{optimal}$=%.3f'%(GAMMA_OPTIMAL))

plt.plot(temperature_SIA_exact   , coord, 'black', ls='--'  , lw=2, alpha=1)

plt.plot(temperature_SIA_exact_1, coord, 'blue', ls='--'  , lw=1.5, alpha=0.7)
plt.plot(temperature_SIA_exact_4 , coord, '#56b000', ls='--'  , lw=1.5, alpha=0.7)

#            label=r'Numerical with exact $v_z=-\frac{\dot M}{4}[(1-\frac{z}{H})^5 + \frac{5z}{H} -1]$')
#plt.plot(temperature_srb, coord, 'b', ls='--', lw=4, alpha=0.7, \
#            label=r'Our solution $v_z=-\dot M(\frac{z} {H})^\gamma, \gamma_{SIA}$=%.3f'%(GAMMA))
plt.title('analytical Robin vs. numerical solution')
plt.xlabel('temperature [$^{\circ}$C]');plt.ylabel('z [m]')
plt.grid(linestyle='dotted')
plt.ylim([0,THICKNESS+100])
plt.xlim([-32,0])
#plt.legend(loc=1)
plt.savefig("results_%s/problem-temperature.png"%FILENAME,dpi=300)
plt.clf()

## plot vertical velocity
vz_1st         = project(vz_1st         , Q).vector().array()[::-1]
vz_SIA_fit     = project(vz_SIA_fit     , Q).vector().array()[::-1]
vz_SIA_exact   = project(vz_SIA_exact   , Q).vector().array()[::-1]
vz_SIA_exact_1= project(vz_SIA_exact_1, Q).vector().array()[::-1]
vz_SIA_exact_4 = project(vz_SIA_exact_4 , Q).vector().array()[::-1]
vz_SIA_optimal = project(vz_SIA_optimal , Q).vector().array()[::-1]

plt.plot(vz_1st        , coord, 'red', lw=2, ls=':' , alpha=1.,\
         label=r'$v_z$ linear (Robin)')
#         label=r'$v_z=-\dot M\frac{z} {H}$ linear (Robin)')
#plt.plot(vz_SIA_fit    , coord, 'g',       lw=2, ls='-' , alpha=0.7,)\
#        label=r'$v_z=-\dot M(\frac{z} {H})^\gamma$, $\gamma$=%.3f (SIA fit)'%(GAMMA))
#plt.plot(vz_SIA_optimal, coord, 'g',       lw=2, ls='-' , alpha=0.7,\
#        label=r'$v_z=-\dot M(\frac{z} {H})^\gamma$, $\gamma$=%.3f'%(GAMMA_OPTIMAL))
#plt.plot(vz_SIA_exact  , coord, 'black',       lw=2, ls='--', alpha=1.,\
#         label=r'$v_z=-\frac{\dot M}{4}[(1-\frac{z}{H})^5 + \frac{5z}{H} -1]$')

plt.plot(vz_SIA_exact_1, coord, 'blue',       lw=1.5, ls='--', alpha=.7,\
         label=r'$v_z$ SIA (n=1)')
plt.plot(vz_SIA_exact   , coord, 'black',       lw=2, ls='--', alpha=1.,\
         label=r'$v_z$ SIA (n=3)')
plt.plot(vz_SIA_exact_4 , coord, '#56b000',       lw=1.5, ls='--', alpha=.7,\
         label=r'$v_z$ SIA (n=4)')

plt.title('vertical velocity')
plt.xlabel('vertical velocity [m yr$^{-1}$]');plt.ylabel('z [m]')
plt.grid(linestyle='dotted')
plt.ylim([0,THICKNESS+100])
plt.legend(loc=1)
plt.savefig("results_%s/problem-vz.png"%FILENAME,dpi=300)
plt.clf()

sys.exit("save the figure for problem and exit")
##############################
#      PLOT GAMMA RANGES
##############################
# our solution with gamma ranges
# compared with the actual solution
_1 = np.linspace(GAMMA_OPTIMAL-.12, GAMMA_OPTIMAL-0.03, 4)
_2 = np.linspace(GAMMA_OPTIMAL    , GAMMA_OPTIMAL+.24 , 9)
#_1 = np.linspace(1.436-.12, 1.436-0.03, 4)
#_2 = np.linspace(1.436, 1.436+.24 , 9)
gamma_range = np.hstack([_1,_2])

cmap = mpl.cm.viridis
for item in gamma_range:
    clr = cmap( (item-gamma_range[0])/(gamma_range[-1]-gamma_range[0]) )
    temperature_SRB = SRB_solution(M_dot_srb=M_dot, mesh=mesh, Q=Q,\
                                        thk=THICKNESS, _gamma=item)
    temperature_SRB = temperature_SRB.vector().array()[::-1]
    kwargs = {"color":clr, "lw":1., "alpha":0.9, "label":r"$\gamma$=%.2f"%item}
    plt.plot(temperature_SRB, coord, **kwargs)
#    plt.legend(loc=1)
    plt.title('new analytical vs. numerical solution')
    plt.xlabel('temperature [$^{\circ}$C]');plt.ylabel('z [m]')
    plt.grid(linestyle='dotted')
    plt.ylim([0,THICKNESS+100])
    plt.xlim([-32,0])
#    plt.ylim([0,250])
#    plt.xlim([-8,-2])

plt.plot(temperature_SIA_exact, coord, 'black', ls='--', lw=2, alpha=1.,\
          label=r'$v_z$ exact')
plt.savefig("results_%s/gamma-range-temp.png"%FILENAME,dpi=300)
#plt.savefig("results_%s/gamma-range-inset.png"%FILENAME,dpi=300)
plt.clf()

cmap = mpl.cm.viridis
for item in gamma_range:
    vz_SIA_optimal = Expression(("-A*pow(x[0],C)/pow(B,C)"), \
                                    A=M_dot, B=THICKNESS, C=item, degree=2)
    clr = cmap( (item-gamma_range[0])/(gamma_range[-1]-gamma_range[0]) )
    vz_SIA_optimal = project(vz_SIA_optimal, Q).vector().array()[::-1]
#    vz_SIA_optimal = vz_SIA_optimal.vector().array()[::-1]
    kwargs = {"color":clr, "lw":1., "alpha":0.9, "label":r"$\gamma$=%.2f"%item}
    plt.plot(vz_SIA_optimal, coord, **kwargs)
    plt.title('vertical velocity')
    plt.xlabel('vertical velocity [m yr$^{-1}$]');plt.ylabel('z [m]')
    plt.grid(linestyle='dotted')
    plt.ylim([0,THICKNESS+100])

plt.plot(vz_SIA_exact, coord, 'black', lw=2,\
         ls='--', alpha=1., label=r'$v_z$ exact')
plt.legend(loc=3, prop={'size':10}, ncol=2)
plt.savefig("results_%s/gamma-range-vz.png"%FILENAME,dpi=300)
plt.clf()
