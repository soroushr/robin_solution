"""
This code takes the mesh2d.xml mesh and calculates
the velocities in x and z direction. The output of
this code will be used as input to advection-diffusion
solver.
"""
import os
import math
import numpy as np
from dolfin import *
import matplotlib.pyplot as plt
from sympy.utilities.codegen import ccode
from sympy import symbols
import sympy as sp

plt.rc('font', family='TeX Gyre Schola')

FILENAME = os.path.splitext(__file__)[0]
#################################
## some constants for glacier ice
rho            = 910.        # kg/m3
gravity        = 9.81        # m/s2
rate_factor    = 1e-7        # kPa-3/yr-1
beta           = 9.8e-8      # K/Pa
kelvin         = 273.15      # K
eps            = 1e-10       # --
heat_capacity  = 2097.       # J/kg/K
latent_fusion  = 335.        # kJ/kg
y2s            = 365*24*3600.
n_glen         = 3.          # exponent of Glen's flow law
M              = 0.3         # m/yr
Length         = 750e3       # m
numElems       = 750


## some vialov constants
A0 = 2.*rate_factor/(n_glen+2.) \
       *(rho*gravity/1000.)**n_glen

_coef = (2.*n_glen+2.)/(n_glen+1.)

H0 = _coef*(M/A0)**(1./n_glen)*Length**(1.+1./n_glen)

# define mesh and function space
mesh = Mesh('./vialov_fenics_gen_mesh2d/mesh2d.xml')
V = FunctionSpace(mesh, 'CG', 2)

#velocity = Expression(("0","-5.*x[1]/1."), degree=1)

#plot(mesh, interactive=True)

def surface_boundary(x):   
    """
    marks the surface boundary with the vialov expression
    for some reason the actual Vialov couldn't mark the
    very last surface vertex so it was forced by the 
    last term in the function's "or"
    """    
    if ( x[1] > 0.98*(H0*(1.-(x[0]/Length)**(1.+1./n_glen))) ** (n_glen/(2.*n_glen+2.)) \
                                                or x[0]>Length-Length/float(numElems) ): 
        return True
    else:
        return False

boundary_parts = FacetFunction('size_t', mesh)
bottom = AutoSubDomain(lambda x: near(x[1], 0.0))
top    = AutoSubDomain(lambda x, boundary_parts: surface_boundary(x) )
left   = AutoSubDomain(lambda x: near(x[0], 0.0))
#right  = AutoSubDomain(lambda x: near(x[0], 1.0))

left  .mark(boundary_parts, 1)
bottom.mark(boundary_parts, 2)
top   .mark(boundary_parts, 3)
#right .mark(boundary_parts, 4)

file = File("results_%s/boundary_parts.pvd"%FILENAME)
file << boundary_parts


#plot(velocity,mesh,
#     wireframe = True,              # use wireframe rendering
#     interactive = True,            # do not hold plot on screen
#     scalarbar = False,             # hide the color mapping bar
#     scale = 1.0,                   # scale the warping/glyphs
#     title = "Fancy plot"           # Set your own title
#     )


