'''
Problem:
    steady-state advection diffusion problem
    with Dirichlet on top and Neumann on bottom
    left and right boundaries are left unmarked

Strong formulation:
    velocity*grad(u) - div(c*grad(u)) = f  in domain
                             u = u0     on top boundary (Dirichlet)
                             du/dn = g  on bottom boundary (Neumann)
Weak formulation:
    F = v*dot(velocity, grad(u))*dx + dot(c*grad(v), grad(u))*dx - f*v*dx - g*v*dsN
    a(u, v) = lhs(F) and L(v) = rhs(F)
    - where v is the test function and dsN indicated the surface normal integration

Variables
    - velocity is prescribed by Expression
    - c  = 1.
    - f  = 0. (no source term to begin with)
    - u0 = Constant
    - g  = Constant
'''

import matplotlib.pyplot as plt
import matplotlib.tri as tri
import os
from dolfin import *
import numpy as np
import math
from some_functions import (
    temperature_robin,
    _plot,
    plot_robin_contour
)

plt.rc('font', family='TeX Gyre Schola')

# get file name
FILENAME = os.path.splitext(__file__)[0]

# Create mesh and function space
mesh = RectangleMesh(Point(0, 0), Point(1, 1), 40, 40, 'crossed')
Q    = FunctionSpace(mesh, "CG", 4)

# Define velocity as vector
# velocity = as_vector([1.0, 1.0])
# (a*x+b) + (c*y/d)
class VelocityExpression(Expression):
    def __init__(self, a, b, c, d):
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.e = e
        self.f = f
    def eval(self, value, x):
        value[0] = self.a*x[0]+self.b
        value[1] = self.c*x[1]+self.d
    def value_shape(self):
        return (2,)

velocity = VelocityExpression(0, 0, -5, 0)

#velocity = Expression(("0","-5.*x[1]/1."), degree=1)

#plot(velocity,mesh,
#     wireframe = True,              # use wireframe rendering
#     interactive = True,            # do not hold plot on screen
#     scalarbar = False,             # hide the color mapping bar
#     scale = 5.0,                   # scale the warping/glyphs
#     title = "Fancy plot"           # Set your own title
#     )
#raw_input("pause here")

c = 0.05 # thermal diffusivity
#c = as_matrix([[0.05, 0],
#               [0, 0.05]])

# Create boundary markers
boundary_parts = FacetFunction('size_t', mesh)
bottom = AutoSubDomain(lambda x: near(x[1], 0.0))
top    = AutoSubDomain(lambda x: near(x[1], 1.0))
left   = AutoSubDomain(lambda x: near(x[0], 0.0))
right  = AutoSubDomain(lambda x: near(x[0], 1.0))

left  .mark(boundary_parts, 1)
bottom.mark(boundary_parts, 2)
top   .mark(boundary_parts, 3)
right .mark(boundary_parts, 4)

# Test and trial functions
u, v = TrialFunction(Q), TestFunction(Q)

# Manufactured right hand side
#f   = Expression('x[0] + cos(x[0]) + cos(x[1]) + sin(x[0]) + sin(x[1])',degree=2)
f          = Constant(0.)   # no source term
g_bottom   = Constant(0.5)  # Neumann heat flux
g_left     = Constant(0.)   # Neumann heat flux
g_right    = Constant(0.)   # Neumann heat flux
u0         = Constant(-5.0) # Dirichlet surface
dsN        = Measure("ds", subdomain_id=2, subdomain_data=boundary_parts)
dsN        = Measure("ds", subdomain_id=1, subdomain_data=boundary_parts)
dsN        = Measure("ds", subdomain_id=4, subdomain_data=boundary_parts)

# Define boundary condition
bcs = DirichletBC(Q, u0, boundary_parts, 3)

# Residual
r = dot(velocity, grad(u)) - div(c*grad(u)) - f

# Galerkin variational problem
F = v*dot(velocity, grad(u))*dx + dot(grad(v), c*grad(u))*dx - f*v*dx \
    - g_bottom*v*dsN(2) - g_left*v*dsN(1) - g_right*v*dsN(4)

# Add SUPG stabilisation terms
vnorm = sqrt(dot(velocity, velocity))
h = CellSize(mesh)
delta = h/(2.0*vnorm)
F += delta*dot(velocity, grad(v))*r*dx

# Create bilinear and linear forms
a = lhs(F)
L = rhs(F)
# Solve
u = Function(Q)
solve(a == L, u, bcs)

#plot(u, interactive = True)
file = File("results_%s/u.pvd"%FILENAME)
file << u


"""
_plot(interpolate(u,Q))
plt.xlim([0,1])
plt.ylim([0,1])
plt.title('temperature distribution')
plt.xlabel('x');plt.ylabel('y')
plt.grid(True)
plt.savefig("results_%s/temperature.png"%FILENAME,dpi=300)
plt.clf()
#plt.show()


ylist = np.linspace(0,1,11)
u_robin = temperature_robin(depths=ylist, T_surface=-5., thickness=1.,
                        ghf=0.5, acc_rate=5., conductivity=0.05,
                        diffusivity=0.05)

# Print out calcuated solution
print 'x--', 'u------------ ', 'Robin--------- ', 'diff--------- '
for i in xrange(0, 11):
    x = [0.5, i/10.0]
    print i/10.0, u(x), u_robin[i], u(x)-u_robin[i] 

## plot ################################
# get array components and triangulation
v = u.compute_vertex_values(mesh)
x = mesh.coordinates()[:,0]
y = mesh.coordinates()[:,1]
t = mesh.cells()

labels = np.linspace(v.min(), v.max(), 5)
cm = plt.get_cmap('jet')
plt.tricontour(x, y, v, 20, linewidths=0.5, colors='k', linestyle='-')
CS = plt.tricontourf(x, y, t, v, 20, vmin = v.min(), vmax = v.max() ,cmap=cm)
plt.colorbar(ticks=labels)
p = plt.triplot(x, y, t, '-', color='k', lw=0.2, alpha=0.1)
plt.xlim([x.min(), x.max()]);
plt.ylim([y.min(), y.max()])
plt.axes().set_aspect('equal')
plt.xlabel('x');plt.ylabel('y')
plt.title('temperature distribution')
plt.savefig("results_%s/temperature_fenics.png"%FILENAME,dpi=300)
plt.clf()
#plt.show()

#xlist = np.linspace(0,1,11)
#ylist = np.linspace(0,1,11)

#CMAP = plt.get_cmap('jet', 10)
#CMAP.set_over('grey')
#colorbar_args = {"pad":"5%", "extend":"max"}
#x_ascii = plot_robin_contour(xlist, ylist, accumulation=5.,
#                    clim=(-5., 0.), clim_step=1,
#                    cbar_label='temperature')

#plt.xlim([0,1])
#plt.ylim([0,1])
#plt.title('temperature Robin')
#plt.xlabel('x');plt.ylabel('y')
#plt.grid(True)
#plt.savefig("results_%s/temperature_robin.png"%FILENAME,dpi=300)
"""
