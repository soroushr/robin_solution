'''
Problem:
    steady-state advection diffusion problem
    with Dirichlet on top and Neumann on bottom
    left and right boundaries are left unmarked

Strong formulation:
    velocity*grad(u) - div(c*grad(u)) = f  in domain
                             u = u0     on top boundary (Dirichlet)
                             du/dn = g  on bottom boundary (Neumann)
Weak formulation:
    F = v*dot(velocity, grad(u))*dx + dot(c*grad(v), grad(u))*dx - f*v*dx - g*v*dsN
    a(u, v) = lhs(F) and L(v) = rhs(F)
    - where v is the test function and dsN indicated the surface normal integration

Variables
    - velocity is prescribed by Expression
    - c  = 1.
    - f  = 0. (no source term to begin with)
    - u0 = Constant
    - g  = Constant
'''

import matplotlib.pyplot as plt
import matplotlib.tri as tri
from matplotlib import colors
import os
from dolfin import *
import numpy as np
import math
from some_functions import (
    temperature_robin,
    _plot,
    plot_robin_contour
)

plt.rc('font', family='TeX Gyre Schola')

# get file name
FILENAME = os.path.splitext(__file__)[0]

# Create mesh and function space
mesh = RectangleMesh(Point(0, 0), Point(1, 1), 40, 40, 'crossed')
Q    = FunctionSpace(mesh, "CG", 1)

# Define velocity as vector
# velocity = as_vector([1.0, 1.0])
# (a*x+b) + (c*y/d)
class VelocityExpression(Expression):
    def __init__(self, a, b, c, d):
        self.a = a
        self.b = b
        self.c = c
        self.d = d
#    def eval(self, value, x):
#        value[0] = self.a*x[0]+self.b
#        value[1] = self.c*x[1]+self.d
    def eval(self, value, x):
        value[0] = self.a*x[0]+self.b
        value[1] = self.c*x[1]+self.d
    def value_shape(self):
        return (2,)

#velocity = VelocityExpression(0,0,-5,0)

#velocity = Expression(("0","-5.*x[1]/1."), degree=1)

#plot(velocity,mesh,
#     wireframe = True,              # use wireframe rendering
#     interactive = True,            # do not hold plot on screen
#     scalarbar = False,             # hide the color mapping bar
#     scale = 5.0,                   # scale the warping/glyphs
#     title = "Fancy plot"           # Set your own title
#     )
#raw_input("pause here")

Pe = Constant(2e0)
c = 1./Pe # thermal diffusivity
#c = as_matrix([[0.05, 0],
#               [0, 0.05]])

# Create boundary markers
boundary_parts = FacetFunction('size_t', mesh)
bottom = AutoSubDomain(lambda x: near(x[1], 0.0))
top    = AutoSubDomain(lambda x: near(x[1], 1.0))
left   = AutoSubDomain(lambda x: near(x[0], 0.0))
right  = AutoSubDomain(lambda x: near(x[0], 1.0))

left  .mark(boundary_parts, 1)
bottom.mark(boundary_parts, 2)
top   .mark(boundary_parts, 3)
right .mark(boundary_parts, 4)

# Test and trial functions
u, v = TrialFunction(Q), TestFunction(Q)

# Manufactured right hand side
#f   = Expression('x[0] + cos(x[0]) + cos(x[1]) + sin(x[0]) + sin(x[1])',degree=2)
f          = Constant(0.)   # no source term
g_bottom   = Constant(2.0)  # Neumann heat flux
g_left     = Constant(0.)   # Neumann heat flux
#g_right    = Constant(0.)   # Neumann heat flux
u0_top     = Constant(-5.0) # Dirichlet surface
u0_right   = Constant(-5.0) # Dirichlet right
dsN        = Measure("ds", subdomain_id=1, subdomain_data=boundary_parts)
dsN        = Measure("ds", subdomain_id=2, subdomain_data=boundary_parts)
#dsN        = Measure("ds", subdomain_id=4, subdomain_data=boundary_parts)

# Define boundary condition
bc_top   = DirichletBC(Q, u0_top  , boundary_parts, 3)
bc_right = DirichletBC(Q, u0_right, boundary_parts, 4)
bcs = [bc_top, bc_right]

def stable_var_form(velocity, f, g_bottom, g_left):
    u, v = TrialFunction(Q), TestFunction(Q)
    # Residual
    r = dot(velocity, grad(u)) - div(c*grad(u)) - f

    # Galerkin variational problem
    F = v*dot(velocity, grad(u))*dx + dot(grad(v), c*grad(u))*dx - f*v*dx \
        - g_bottom*v*dsN(2) - g_left*v*dsN(1)

    # Add SUPG stabilisation terms
    vnorm = sqrt(dot(velocity, velocity))
    h = CellSize(mesh)
    delta = h/(2.0*vnorm)
    F += delta*dot(velocity, grad(v))*r*dx

    # Create bilinear and linear forms
    a = lhs(F)
    L = rhs(F)

    return a, L


MAX = 10
for i in [x+1 for x in range(MAX)]:
    j = -i
    velocity = VelocityExpression(i, 0., j, 0.)

    a, L = stable_var_form(velocity, f, g_bottom, g_left)

    u = Function(Q)
    solve(a == L, u, bcs)

    file = File("results_%s/u_%i.pvd"%(FILENAME,j))
    file << u
    print "M=%i solved"%i

    ## =====================================
    ## plot
    """
    cmap = plt.get_cmap('jet',20)
#    cmap.set_under('grey')
#    cmap.set_over('black')

    v = u.compute_vertex_values(mesh)
    x = mesh.coordinates()[:,0]
    y = mesh.coordinates()[:,1]
    t = mesh.cells()

    n_lvls = 9
    levels = np.linspace(-5,-3,n_lvls)
    labels = [-5,-4.5,-4.,-3.5,-3]

    plt.tricontour(x, y, v, levels=levels, linewidths=0.3, colors='k', linestyle='-')
    cs = plt.tricontourf(x, y, t, v, n_lvls, levels=levels, cmap=cmap, extend="both")
    cbar = plt.colorbar(cs)
    cbar.set_ticks(labels)
    cbar.set_ticklabels(labels)

    p = plt.triplot(x, y, t, '-', color='k', lw=0.2, alpha=0.1)

    plt.xlim([x.min(), x.max()]);
    plt.ylim([y.min(), y.max()])
    plt.axes().set_aspect('equal')
    plt.xlabel('x');plt.ylabel('y')
    plt.title('temperature distribution')
    plt.savefig("results_%s/vx_vy%i.png"%(FILENAME,i),dpi=300)
    plt.clf()
    """

    ## =========================
    ## extract values at the bed
    dofmap  = Q.dofmap()
    dof_x   = dofmap.tabulate_all_coordinates(mesh).reshape((Q.dim(), -1))
#    dof_x   = Q.tabulate_all_coordinates().reshape((Q.dim(), mesh.geometry().dim()))
    xbed    = dof_x[:,0] # 0 is columnar and 1 is along the bed
#    indices = np.where(xbed < DOLFIN_EPS)[0]
    indices = np.where(np.logical_and(xbed < 0.5+DOLFIN_EPS, xbed > 0.5-DOLFIN_EPS))[0]
    print indices.shape
    xs      = dof_x[indices]
    vals    = u.vector()[indices]

    cmap_b= plt.get_cmap('viridis',20)
    color = cmap_b(float(i)/float(MAX))
#    plt.plot(xs[:,0],vals,color=color,lw=2,alpha=0.7, label='$v_x$=%i'%i)
    plt.plot(vals,xs[:,1],color=color,lw=2,alpha=0.7, label='$v_x$=%i'%i)
    plt.grid(True);
    plt.xlabel('x')
    plt.ylabel('temperature')
    plt.title('basal temperature')
    box = plt.gca().get_position()
    plt.gca().set_position([0.125, 0.18, 0.775, 0.72])
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
          fancybox=False, shadow=False, ncol=5)
#    plt.legend(loc=1)
#    plt.savefig('results_%s/basal_temperature%i.png'%(FILENAME,i),dpi=300)
    plt.savefig('results_%s/vertical_temperature_middle%i.png'%(FILENAME,i),dpi=300)
#    plt.clf()

#    coords = mesh.coordinates() 
#    xbasal = coords[coords[:,1]==0]  


"""
ylist = np.linspace(0,1,11)
u_robin = temperature_robin(depths=ylist, T_surface=-5., thickness=1.,
                        ghf=0.5, acc_rate=5., conductivity=0.05,
                        diffusivity=0.05)

#xlist = np.linspace(0,1,11)
#ylist = np.linspace(0,1,11)

#CMAP = plt.get_cmap('jet', 10)
#CMAP.set_over('grey')
#colorbar_args = {"pad":"5%", "extend":"max"}
#x_ascii = plot_robin_contour(xlist, ylist, accumulation=5.,
#                    clim=(-5., 0.), clim_step=1,
#                    cbar_label='temperature')

#plt.xlim([0,1])
#plt.ylim([0,1])
#plt.title('temperature Robin')
#plt.xlabel('x');plt.ylabel('y')
#plt.grid(True)
#plt.savefig("results_%s/temperature_robin.png"%FILENAME,dpi=300)
"""
