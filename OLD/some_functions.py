import matplotlib.pyplot as plt
import matplotlib.tri as tri
import os
from dolfin import *
import numpy as np
import math

plt.rc('font', family='TeX Gyre Schola')

## some constants for glacier ice
rho            = 910.        # kg/m3
gravity        = 9.81        # m/s2
conductivity   = 2.10        # W/m/K
diffusivity_y  = 34.4        # m2/yr
diffusivity_s  = 1.09e-6     # m2/s
rate_factor    = 5e-8        # kPa-3/yr-1
beta           = 9.8e-8      # K/Pa
kelvin         = 273.15      # K
eps            = 1e-10       # --
heat_capacity  = 2097.       # J/kg/K
latent_fusion  = 335.        # kJ/kg
s2y            = 365*24*3600.

## the Robin solution
def temperature_robin(depths ,T_surface, thickness, ghf,
                      acc_rate, conductivity, diffusivity):
    """Function to calculate the Robin solution with depth
       given the constants and boundary conditions values

       Args:
            depths: an array of depth values where the temperature
                    is to be calculated
            T_surface: surface temperature in centigrade
            thickness: ice thickness
            ghf: geothermal heat flux as Neumann BC at the base
            acc_rate: accumulation rate on the surface
            conductivity: conductivity
            diffusivity: diffusivity

       Return:
            results: a list of calculated temperatures
    """
    _q_sqrd = abs(acc_rate/2./(thickness+DOLFIN_EPS)/diffusivity)
    q_term  = np.sqrt(_q_sqrd)

    def erff(z):
        error = math.erf(z*q_term)-math.erf(thickness*q_term)
        return error

    results = []
    for depth in depths:
        robin = ghf*np.sqrt(math.pi)/2./conductivity/q_term*(erff(depth)+DOLFIN_EPS)
        temperature_z = T_surface - robin
        results.append(temperature_z)
    results = np.asarray(results)

    return results


## functions for plotting with matplotlib
def mesh2triang(mesh):
    xy = mesh.coordinates()
    return tri.Triangulation(xy[:, 0], xy[:, 1], mesh.cells())

def _plot(obj):
    plt.gca().set_aspect('equal')
    if isinstance(obj, Function):
        mesh = obj.function_space().mesh()
        if (mesh.geometry().dim() != 2):
            raise(AttributeError)
        if obj.vector().size() == mesh.num_cells():
            C = obj.vector().array()
            plt.tripcolor(mesh2triang(mesh), C)
        else:
            C = obj.compute_vertex_values(mesh)
            plt.tripcolor(mesh2triang(mesh), C, shading='gouraud')
    elif isinstance(obj, Mesh):
        if (obj.geometry().dim() != 2):
            raise(AttributeError)
        plt.triplot(mesh2triang(obj), color='k')

## function for plotting the Robin solution results
def plot_robin_contour(xlist, ylist, accumulation,
                    clim=(-5., 0.), clim_step=1,
                    colorbar_args={}, pcolor_args={}, cbar_label='temperature'):
    """ 

    """
    x, y = np.meshgrid(xlist, ylist)

    t_ascii = []
    for item in xlist:
        temperature = temperature_robin(ylist, T_surface=-5., thickness=1.,
                        ghf=0.5, acc_rate=5., conductivity=1.,
                        diffusivity=1.)
        t_ascii.append(temperature)
    t_ascii = np.asarray(t_ascii).T
#    return (t_ascii.T)
    cs = plt.contourf(x, y, t_ascii, **pcolor_args)

    cbar = plt.colorbar(cs, **colorbar_args)
    cbar.set_label(cbar_label)
    labels = range(int(clim[0]), int(clim[1]) + 1, clim_step)
    print labels
    cbar.set_ticks(labels)
    cbar.set_ticklabels(labels)
#    plt.clim(*clim)
