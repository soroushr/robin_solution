import os
import math
import numpy as np
import matplotlib.pyplot as plt

plt.rc('font', family='TeX Gyre Schola')

FILENAME = os.path.splitext(__file__)[0]
#################################
## some constants for glacier ice
rho            = 910.        # kg/m3
gravity        = 9.81        # m/s2
rate_factor    = 1e-7        # kPa-3/yr-1
beta           = 9.8e-8      # K/Pa
kelvin         = 273.15      # K
eps            = 1e-10       # --
heat_capacity  = 2097.       # J/kg/K
latent_fusion  = 335.        # kJ/kg
s2y            = 365*24*3600.
n_glen         = 3.          # exponent of Glen's flow law

##############################
## Vialov function definitions
def _A0_vialov(rate_factor, *args):
    """Args
        - rate factor: [kPa^-3 yr^-1]
        - n_glen: Glen's flow law=3
        - rho: ice density [kg m^-3]
        - gravity: [m s^-2]

       Returns
        - the A0 constant in vdv 5.31
    """
    _A0 = 2.*rate_factor/(n_glen+2.) \
            *(rho*gravity/1000.)**3
    return _A0

def H0_vialov(M, Length, rate_factor,*args):
    """Args
        - M: accumulation rate [m yr^-1]
        - Length: ice sheet length [m]
        - rate_factor: [kPa^-3 yr^-1]

       Returns
        - left hand side of vdv 5.38
    """
    A0 = _A0_vialov(rate_factor=rate_factor)
    _coef = (2.*n_glen+2.)/(n_glen+1.)
    H0 = _coef*(M/A0)**(1./n_glen)*Length**(1.+1./n_glen)
    return H0

def H_vialov(x, H0, Length, *args):
    """Args
        - x: the location where Vialov is desired [m]
             zero is at the ice divide
        - H0: the result of H0_vialov function
        - Length: [m] must be the same as used in the 
                  H0_vialov function

       Returns
        - left hand side of vdv 5.38
    """
    _ = H0*(1.-(x/Length)**(1.+1./n_glen))
    thickness = _**(n_glen/(2.*n_glen+2.))
    return thickness

def vx_surface_vialov(vialov, dx, rate_factor, *args):
    """Args
        - vialov: an array that has the Vialov profile
            calculated from the H_vialov function [m]
        - dx: the dx for calculating slope [m]
        - rate_factor: [kPa^-3 yr^-1]

       Returns
        - vx_surface: array of velocity
            calculated at the surface [m yr^-1]
    """
    grad = -np.gradient(vialov,edge_order=2)/dx
    tau_dx = rho*gravity*vialov*grad/1000.
    vx_surface = 2.*rate_factor*vialov/(n_glen+1.) \
                   *tau_dx**n_glen + 0 # 0 is for sliding vel

    # FIXME: last element force to be nonzero
    # which was caused by the gradient function
    vx_surface[-1] = vx_surface[-2]*1.2
    return vx_surface

def vx_at_depth(thickness, vx_surface, nz=50, *args):
    """Args
        - thickness: ice thickness [m]
        - vx_surface: surface velocity [m]
        - ny: number of vertical discretizations
            default set to 50

       Returns
        - zs: array of the discretized vertical
            direction with the same ny and thickness [m]
        - vx_at_depth: array of vx as a function of 
            depth [m/yr]
    """
    zs = np.linspace(0, thickness, ny)
    vx_at_depth = []
    for z in zs:
        vx_z = vx_surface*(1.-(z/thickness-1.)**4.)
        vx_at_depth.append(vx_z)

    return zs, np.asarray(vx_at_depth)

####################
## results and plots
Length  = 750e3
dx = 750
M = 0.3
H0_test = H0_vialov(M=M, Length=Length, rate_factor=rate_factor)

x_axis = np.linspace(0,Length,dx)

# vialov profile
profile = []
for i in x_axis:
    profile.append(H_vialov(x=i, H0=H0_test, Length=Length))
profile = np.asarray(profile)

#plt.plot(x_axis/1000., profile, linestyle='-', color='g', linewidth=3)
#plt.xlabel('ice sheet length (km)')
#plt.ylabel('ice sheet elevation (m)')
#plt.xlim([0, (Length+50*dx)/1000.])
#plt.ylim([0, math.ceil(np.max(profile)/1000.0)*1000.0])
#plt.grid(True)
#plt.title('Vialov profile with M=%.2f [m/yr] and L=%i [km]'%(M,Length/1000.))
#plt.savefig('results_%s/Vialov.png'%FILENAME,dpi=300)
#plt.show()

## surface velocity from vialov profile
vx = vx_surface_vialov(vialov=profile, dx=dx, rate_factor=rate_factor)
#plt.plot(x_axis/1000.,vx, linestyle='-', color='red', linewidth=3)
#plt.xlabel('ice sheet length (km)')
#plt.ylabel('surface velocity (m/yr)')
#plt.xlim([0, (Length+50*dx)/1000.])
#plt.grid(True)
#plt.title('Surface velocity of Vialov profile with M=%.2f [m/yr] and L=%i [km]'%(M,Length/1000.))
#plt.savefig('results_%s/surface-velocity.png'%FILENAME,dpi=300)
##plt.show()
#plt.clf()

## x-direction velocity at depth from surface velocity
zs, vx_at_depth = vx_at_depth(profile[-10], vx[-10])
#plt.plot(vx_at_depth, zs, linestyle='-', color='k', linewidth=3)
#plt.xlabel('x-direction velocity (m/yr)')
#plt.ylabel('depth (m)')
#plt.grid(True)
#plt.title('x-direction velocity at depth %i km from the divide'%(x_axis[-10]/1000.))
#plt.savefig('results_%s/x-dir-velocity-at-depth.png'%FILENAME,dpi=300)
##plt.show()
#plt.clf()
