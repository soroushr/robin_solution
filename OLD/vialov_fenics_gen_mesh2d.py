"""
This file solves the Vialov profile using FEniCS in
1D. When the surface elevation (thickness) of the 
profile is obtained, it is projected onto a 2D mesh
and saved as a mesh.xml object.

This mesh is then used in an other code to calculate
velocities in x and z directions that feed the code
for adv-diffusion solution
"""
import os
import math
import numpy as np
from dolfin import *
import matplotlib.pyplot as plt
from sympy.utilities.codegen import ccode
from sympy import symbols
import sympy as sp
import mshr

plt.rc('font', family='TeX Gyre Schola')

FILENAME = os.path.splitext(__file__)[0]
#################################
## some constants for glacier ice
rho            = 910.        # kg/m3
gravity        = 9.81        # m/s2
rate_factor    = 1e-7        # kPa-3/yr-1
beta           = 9.8e-8      # K/Pa
kelvin         = 273.15      # K
eps            = 1e-10       # --
heat_capacity  = 2097.       # J/kg/K
latent_fusion  = 335.        # kJ/kg
y2s            = 365*24*3600.
n_glen         = 3.          # exponent of Glen's flow law
M              = 0.3         # m/yr
Length         = 750e3       # m
numElems       = 750

## some vialov constants
A0 = 2.*rate_factor/(n_glen+2.) \
       *(rho*gravity/1000.)**n_glen

_coef = (2.*n_glen+2.)/(n_glen+1.)

H0 = _coef*(M/A0)**(1./n_glen)*Length**(1.+1./n_glen)

## symbolic declarations
x, y, vx, vy = symbols('x[0], x[1], vx, vy')

H = (H0*(1.-(x/Length)**(1.+1./n_glen))) \
                **(n_glen/(2.*n_glen+2.))

dHdx   = sp.diff(H, x)
tau_dx = -rho*gravity*H*dHdx/1000.

vx_surface = 2.*rate_factor*H/(n_glen+1.) \
                   *tau_dx**n_glen + 0


# store sym expressions
H_copy          = H
dHdx_copy       = dHdx
tau_dx_copy     = tau_dx
vx_surface_copy = vx_surface

# return dolfin expression of a sympy equation
def sym_to_fenics(var):
    return Expression(ccode(var).replace('M_PI', 'pi'))

dHdx       = sym_to_fenics(dHdx)
H          = sym_to_fenics(H)
tau_dx     = sym_to_fenics(tau_dx)
vx_surface = sym_to_fenics(vx_surface)

# define mesh and function space
mesh = IntervalMesh(numElems, 0.0, Length)
V = FunctionSpace(mesh, 'CG', 2)

# interpolate to functionspace
dHdx_V       = interpolate(dHdx, V)
H_V          = interpolate(H, V)
tau_dx_V     = interpolate(tau_dx, V)
vx_surface_V = interpolate(vx_surface, V)

#p = plot(tau_dx_V, rescale=True, interactive=True)

#file = File("results_%s/H.pvd"%FILENAME)
#file << H_V

########
## plot:
# (sometimes) skip the last element for plotting
# because the differentiation screws it up
plot_var = vx_surface_V.compute_vertex_values(mesh)
x1d     = mesh.coordinates()[:,0]

plt.plot(x1d/1000., plot_var, 'g--', lw=3, alpha=0.7, label='FEniCS Vialov solution')
plt.title('FEniCS Vialov')
plt.xlabel('length [km]');plt.ylabel('surface velocity [m/yr]')
plt.xlim([0, 800])
plt.grid(True)
plt.legend(loc=1)
#plt.savefig("results_%s/vx_surface.png"%FILENAME,dpi=300)
#plt.show()

####################################
## 2D mesh with changing coordiantes
nalong = 750
ndepth = 20
mesh2d = RectangleMesh(Point(0., 0.), Point(Length, 1), nalong, ndepth, "left")

x2d = mesh2d.coordinates()[:,0]
y2d = mesh2d.coordinates()[:,1]

# a function to project every x,y coordinate
# of a rectangle mesh2d to the x,y coord of Vialov
def Vialov(a, b):
    elevs = [float(H_copy.subs(x, i)) for i in a]
    elevs = np.asarray(elevs)
    return [a, b*elevs]

x_bar, y_bar = Vialov(x2d, y2d)
xy_bar_coor = np.array([x_bar, y_bar]).transpose()
mesh2d.coordinates()[:] = xy_bar_coor
plot(mesh2d, title="stretched mesh", interactive=True)

mesh_file = File("results_%s/mesh2d.xml"%FILENAME)
mesh_file << mesh2d

#######################

## FIXME: THIS DOESN'T WORK BUT USEFUL 
## TO KEEP FOR NOW

## 2D mesh with mshr
## create a ccw of points for Vialov
#H_vialov = H_V.compute_vertex_values(mesh)

#forward   = zip(x,np.zeros(x.shape))
#forward   = np.asarray(forward)
#backward  = zip(x[::-1],H_vialov[::-1])
#backward  = np.asarray(backward)
#points    = np.vstack([forward,backward])

## map the coordinates to Point object
#geometry = map(Point,points[:,0],points[:,1])
#domain   = mshr.Polygon(geometry)
#mesh2d   = mshr.generate_mesh(domain,100)
#plot(mesh2d, interactive=True, rescale=True)
