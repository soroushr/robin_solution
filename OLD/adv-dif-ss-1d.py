'''
Problem:
    steady-state advection diffusion problem
    with Dirichlet on top and Neumann on bottom
    left and right boundaries are left unmarked

Strong formulation:
    velocity*grad(u) - div(c*grad(u)) = f  in domain
                             u = u0     on top boundary (Dirichlet)
                             du/dn = g  on bottom boundary (Neumann)
Weak formulation:
    F = v*dot(velocity, grad(u))*dx + dot(c*grad(v), grad(u))*dx - f*v*dx - g*v*dsN
    a(u, v) = lhs(F) and L(v) = rhs(F)
    - where v is the test function and dsN indicated the surface normal integration

Variables
    - velocity is prescribed by Expression
    - c  = 1.
    - f  = 0. (no source term to begin with)
    - u0 = Constant
    - g  = Constant
'''

import matplotlib.pyplot as plt
import matplotlib.tri as tri
import os
from dolfin import *
import numpy as np
import math
from some_functions import (
    temperature_robin,
    _plot,
    plot_robin_contour
)

plt.rc('font', family='TeX Gyre Schola')

# get file name
FILENAME = os.path.splitext(__file__)[0]

# Create mesh and function space
mesh = UnitIntervalMesh(1000) #RectangleMesh(Point(0, 0), Point(1, 1), 40, 40, 'crossed')
Q = FunctionSpace(mesh, "CG", 1)

## Define velocity as vector
velocity = Expression(("-5.*x[0]/1."), degree=2)

c = 0.05 # thermal diffusivity

# Create boundary markers
boundary_parts = FacetFunction('size_t', mesh)
bottom = AutoSubDomain(lambda x: near(x[0], 0.0))
top    = AutoSubDomain(lambda x: near(x[0], 1.0))
bottom.mark(boundary_parts, 2)
top   .mark(boundary_parts, 3)

# Test and trial functions
u, v = TrialFunction(Q), TestFunction(Q)

# Manufactured right hand side
f   = Constant(0.)    # no source term
g   = Constant(0.5)   # Neumann heat flux
u0  = Constant(-5.0)  # Dirichlet surface
dsN = Measure("ds", subdomain_id=2, subdomain_data=boundary_parts)

# Residual
r = velocity * u.dx(0) - c*u.dx(0).dx(0) - f

# Galerkin variational problem
F = v*(velocity * u.dx(0))*dx + c*(v.dx(0) * u.dx(0))*dx - f*v*dx - g*v*dsN

# Add SUPG stabilisation terms
vnorm = sqrt(velocity * velocity)
h = CellSize(mesh)
delta = h/(2.0*vnorm)
F += delta*(velocity * v.dx(0))*r*dx

# Create bilinear and linear forms
a = lhs(F)
L = rhs(F)

# Define boundary condition
bcs = DirichletBC(Q, u0, boundary_parts, 3)

# Solve
u = Function(Q)
solve(a == L, u, bcs)

#plot(u, interactive = True)
file = File("results_%s/u.pvd"%FILENAME)
file << u

#################################
# plot:
import matplotlib.pyplot as plt
plt.rc('font', family='TeX Gyre Schola')

u_fem = u.vector().array()[::-1]

t = mesh.coordinates()[:,0]

u_robin = temperature_robin(depths=t, T_surface=-5., thickness=1.,
                        ghf=0.5, acc_rate=5., conductivity=0.05,
                        diffusivity=0.05)

#print u_fem.shape, t.shape
plt.plot(u_fem, t, 'r', lw=5, alpha=0.7, label='FEniCS solution')
plt.plot(u_robin, t, 'g--', lw=5, alpha=0.7, label='Robin solution')
plt.title('FEM vs the Robin solution')
plt.xlabel('temperature');plt.ylabel('z')
plt.grid(True)
plt.legend(loc=1)
#plt.savefig("results_%s/temperature.png"%FILENAME,dpi=300)
plt.show()


## Print out calcuated solution
#print 'x--', 'u------------ ', 'Robin--------- ', 'diff--------- '
#for i in xrange(0, 11):
#    x = [0.5, i/10.0]
#    print i/10.0, u(x), u_robin[i], u(x)-u_robin[i]
