"""
This code creates a Vialov ice sheet profile and calculate
the corresponding x and z velocities. The velocities are 
then used to solve the advection-diffusion system with
prescribed surface temperature and lapse rate as Dirichlet
boundary condition and geothermal heat flux as Neumann
condition at the bed.
Note - sympy 0.7.6 is required - ccode function will change
"""
import os
import numpy as np
from dolfin import *
import matplotlib.pyplot as plt
from sympy.utilities.codegen import ccode
from sympy import symbols
import sympy as sp

plt.rc('font', family='TeX Gyre Schola')

FILENAME = os.path.splitext(__file__)[0]
#################################
## some constants for glacier ice
lapse_rate     = 7.1/1000.   # degrees per meter
conductivity   = 2.10        # W/m/K
diffusivity    = 34.4        # m2/yr
surface_temp   = -10.        # K
rho            = 910.        # kg/m3
gravity        = 9.81        # m/s2
rate_factor    = 1e-7        # kPa-3 yr-1
beta           = 9.8e-8      # K/Pa
kelvin         = 273.15      # K
eps            = 1e-10       # --
heat_capacity  = 2097.       # J/kg/K
latent_fusion  = 335.        # kJ/kg
y2s            = 365*24*3600.
n_glen         = 3.          # exponent of Glen's flow law
M_dot          = 0.3         # m/yr
Length         = 750e3       # m
numElems       = 750
GHF            = 0.05        # W/m2

## some vialov constants
A0 = 2.*rate_factor/(n_glen+2.) \
       *(rho*gravity/1000.)**n_glen

_coef = (2.*n_glen+2.)/(n_glen+1.)

H0 = _coef*(M_dot/A0)**(1./n_glen)*Length**(1.+1./n_glen)

## symbolic declarations
x, y, vx, vy = symbols('x[0], x[1], vx, vy')

H          = (H0*(1.-(x/Length)**(1.+1./n_glen))) \
                **(n_glen/(2.*n_glen+2.))
dHdx       = sp.diff(H, x)
tau_dx     = -rho*gravity*H*dHdx/1000.
vx_surface = 2.*rate_factor*H/(n_glen+1.) \
                   *tau_dx**n_glen + 0
vz_surface = vx_surface*dHdx - M_dot

# store sym expressions
H_copy          = H
dHdx_copy       = dHdx
tau_dx_copy     = tau_dx
vx_surface_copy = vx_surface
vz_surface_copy = vz_surface

# return dolfin expression of a sympy equation
def sym_to_fenics(var):
    return Expression(ccode(var).replace('M_PI', 'pi'))

dHdx       = sym_to_fenics(dHdx)
H          = sym_to_fenics(H)
tau_dx     = sym_to_fenics(tau_dx)
vx_surface = sym_to_fenics(vx_surface)
vz_surface = sym_to_fenics(vz_surface)

# define mesh and function space
mesh = IntervalMesh(numElems, 0.0, Length)
V = FunctionSpace(mesh, 'CG', 2)

# interpolate to functionspace
dHdx_V       = interpolate(dHdx, V)
H_V          = interpolate(H, V)
tau_dx_V     = interpolate(tau_dx, V)
vx_surface_V = interpolate(vx_surface, V)
vz_surface_V = interpolate(vz_surface, V)

#p = plot(tau_dx_V, rescale=True, interactive=True)

#file = File("results_%s/H.pvd"%FILENAME)
#file << H_V

########
## plot:
# (sometimes) skip the last element for plotting
# because the differentiation screws it up
plot_var = vz_surface_V.compute_vertex_values(mesh)
x1d     = mesh.coordinates()[:,0]

plt.plot(x1d/1000., plot_var, 'g--', lw=3, alpha=0.7, label='FEniCS Vialov solution')
plt.title('FEniCS Vialov')
plt.xlabel('length [km]');plt.ylabel('surface velocity [m/yr]')
plt.xlim([0, 800])
plt.grid(True)
plt.legend(loc=1)
#plt.savefig("results_%s/vx_surface.png"%FILENAME,dpi=300)
#plt.show()
plt.clf()
####################################
## 2D mesh with changing coordiantes
nalong = numElems
ndepth = 20
mesh2d = RectangleMesh(Point(0., 0.), Point(Length, 1), nalong, ndepth, "left")

mesh2d_copy = mesh2d
V2d_copy    = FunctionSpace(mesh2d_copy, "CG", 1)

nn = V2d_copy.dim()                                                                      
dd = mesh2d_copy.geometry().dim()                                                        

dof_coordinates = V2d_copy.dofmap().tabulate_all_coordinates(mesh2d_copy)                      
dof_coordinates.resize((nn, dd))                                                   
dof_xx = dof_coordinates[:, 0]                                                    
dof_yy = dof_coordinates[:, 1]                                                    

x2d = mesh2d.coordinates()[:,0]
y2d = mesh2d.coordinates()[:,1]
#y2d_copy = y2d

# a function to project every x,y coordinate
# of a rectangle mesh2d to the x,y coord of Vialov
# set the minimum thickness to 5 to have a right
# boundary for perscribing right BC. Otherwise fails
def vialov_mesh(a, b):
    elevs   = [float(H_copy.subs(x, i)) for i in a]
    elevs   = np.asarray(elevs)
    elevs [ elevs<10 ] = 5.
    return [a, b*elevs]

x_bar, y_bar = vialov_mesh(x2d, y2d)
xy_bar_coor = np.array([x_bar, y_bar]).transpose()
mesh2d.coordinates()[:] = xy_bar_coor

# save mesh
#mesh_file = File("results_%s/mesh2d.xml"%FILENAME)
#mesh_file << mesh2d

# new function space based on the new mesh
V2d = FunctionSpace(mesh2d, "CG", 1)

tau       = Function(V2d)
u_surface = Function(V2d)
w_surface = Function(V2d)
thickness = Function(V2d)
u_depth   = Function(V2d)
w_depth   = Function(V2d)

# dofs for finding indices 
dofmap  = V2d.dofmap()
dof_x   = dofmap.tabulate_all_coordinates(mesh2d).reshape((V2d.dim(), -1))

# coordinates of the old mesh used to find x-direction
# intervals and corresponding indices
coords = mesh.coordinates()

# calculate driving stress from sym expression at any
# given length discretization then apply the same 
# value to the whole columns of the structured mesh
for coord in coords:
    # this variable is just to set the "margin" for defining
    # the "near" at nodes - that division by 4 is arbitrary
    border = Length/float(numElems)/4.
    # 0 is columnar and 1 is along the bed
    # this is only possible because the mesh is structured
    xbed    = dof_x[:,0]
    indices = np.where(np.logical_and(xbed < coord+border, xbed > coord-border))[0]

    def _constant_at_depth(expression):
        """Projects a constant value that is calculated from
           lamellar flow at the surface to all the nodes at
           a given column of nodes. The returned function will
           have constant values at depth.
        """
        _local = expression.subs(x, coord)
        _list  = [_local*i for i in np.ones([ndepth+1, 1])]
        _list  = np.asarray(_list)
        _list  = np.where(_list==sp.zoo, 400., _list)
        _list  = np.where(_list==sp.nan, 840., _list)
        return _list

    tau.vector()[indices]       = _constant_at_depth(tau_dx_copy)
    thickness.vector()[indices] = _constant_at_depth(H_copy)
    u_surface.vector()[indices] = _constant_at_depth(vx_surface_copy)
    w_surface.vector()[indices] = _constant_at_depth(vz_surface_copy)


def print_min_max(varname, variable):
    maxx = variable.vector().max()
    minn = variable.vector().min()
    print ':::::::::::::::::::::::::::::::'
    print 'max %s is %f' %(varname, maxx)
    print 'min %s is %f' %(varname, minn)
    print ':::::::::::::::::::::::::::::::'

def save_file(varname, variable):
    """
    writes variable to varname (string) in the FILENAME directory
    """
    file = File("results_%s/%s.pvd"%(FILENAME,varname))
    file << variable
    print '%s saved to file'%varname
    print_min_max(varname, variable)

# set the values at the ice divide the same as
# the second column of the mesh - problem is
# with how sympy evaluates gradients at the 
# end points
idx_0 = np.where(np.logical_and(xbed < 0+border, xbed > 0-border))[0]
idx_1 = np.where(np.logical_and(xbed < Length/float(numElems)+border, xbed > Length/float(numElems)-border))[0]

for i, j in zip(idx_0, idx_1):
    _ = w_surface.vector()[j]
    w_surface.vector()[i] = float(_)

vx_temp = u_surface.vector()[:]*(1.-(1.-dof_yy)**(n_glen+1))
u_depth.vector()[:] = vx_temp

vz_temp = w_surface.vector()[:]*(1.-(1.-dof_yy)\
                *((n_glen+2.)/(n_glen+1.)-(1.-dof_yy)**(n_glen+1.)/(n_glen+1.)))
#vz_temp = np.where(vz_temp>0, 0, vz_temp)
#vz_temp = np.where(vz_temp<-M, -M, vz_temp)
w_depth.vector()[:] = vz_temp

#save_file('w_depth'  , w_depth)
#save_file('w_surface', w_surface)
#save_file('u_surface', u_surface)
#save_file('u_depth'  , u_depth)
"""
## plot the velocities
def plot_vialov(variable, lowlim, uplim, title, 
                        colorbar_args = {}, **kwargs):

    v = variable.compute_vertex_values(mesh2d)
    x = mesh2d.coordinates()[:,0]
    y = mesh2d.coordinates()[:,1]
    t = mesh2d.cells()

    clim_step=(uplim-lowlim)/20.
    plt.tricontour(x, y, v, 100, linewidths=0.5, colors='k')
    cm = plt.get_cmap('jet', 20)

    clim=(lowlim, uplim)
    sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=clim[0], vmax=clim[1]))
    sm._A = []
    cbar = plt.colorbar(sm)

    cs = plt.tricontourf(x, y, t, v, 100, vmin = lowlim, vmax = uplim, cmap=cm)
    cm.set_over('grey')

#    cbar = plt.colorbar(cs, **colorbar_args)
#    labels = range(int(clim[0]), int(clim[1]) + 1, clim_step)
    labels = np.linspace(clim[0], clim[1], clim_step)
    cbar.set_ticks(labels)
    cbar.set_ticklabels(labels)
    plt.xlim([x.min(), x.max()]);
    plt.ylim([y.min(), y.max()])
    plt.axes().set_aspect(100)
    plt.xlabel('x [m]');plt.ylabel('z [m]')
    plt.title(title)

colorbar_args = {"extend":"max"}
plot_vialov(variable=u_depth, lowlim=0, uplim=100, title='$v_x$ [m/yr]',
                colorbar_args=colorbar_args)
plt.savefig("results_%s/u.png"%FILENAME, dpi=200)
plt.clf()

colorbar_args = {"extend":"min"}
# FIXME: this doesn't work
plot_vialov(variable=w_depth, lowlim=-0.3, uplim=0, title='$v_z$ [m/yr]',
                colorbar_args=colorbar_args)
plt.savefig("results_%s/w.png"%FILENAME, dpi=200)
plt.clf()
"""

### define mesh boundary ###
#def surface_boundary(x):   
#    """
#    marks the surface boundary with the vialov expression
#    for some reason the actual Vialov couldn't mark the
#    very last surface vertex so it was forced by the 
#    last term in the function's "or"
#    """
#    if ( x[1] > 0.98*(H0*(1.-(x[0]/Length)**(1.+1./n_glen))) ** (n_glen/(2.*n_glen+2.)) \
#                                                or x[0]>Length-Length/float(numElems) ):
#        return True
#    else:
#        return False


## first mark all nodes as zero, then mark all 
## the exteriors as surface(3) (because surface is
## complicated) then overwrite bed(2), divide(1), and 
## the tiny right boundary(4)
boundary_parts = FacetFunction('size_t', mesh2d)
boundary_parts.set_all(0)
for f in facets(mesh2d):
  if any(ff.exterior() for ff in facets(f)):
    boundary_parts[f] = 3

bottom = AutoSubDomain(lambda x: near(x[1], 0.0))
#top    = AutoSubDomain(lambda x, boundary_parts: surface_boundary(x) )
left   = AutoSubDomain(lambda x: near(x[0], 0.0))
right  = AutoSubDomain(lambda x: near(x[0], Length))

left  .mark(boundary_parts, 1)
bottom.mark(boundary_parts, 2)
#top   .mark(boundary_parts, 3)
right .mark(boundary_parts, 4)

#save_file('boundary_parts', boundary_parts)

## the velocity that would be fed into the
## Robin solution
w_depth_Robin = Function(V2d)
depth_exp = Expression("x[1]", degree=1)
depth = project(depth_exp, V2d)
_w_depth_Robin = -M_dot*depth/thickness
w_depth_Robin = project(_w_depth_Robin, V2d)

# assemble velocity as vector
#velocity = as_vector([u_depth, w_depth])
velocity = as_vector([u_depth, w_depth_Robin])

###################################
######### SOLUTION ################
bottom_gradient = GHF * (diffusivity/conductivity)

f          = Constant(0.)                            # no source term
g_bottom   = Constant(bottom_gradient)               # Neumann heat flux
g_left     = Constant(0.)                            # Neumann heat flux
#g_right    = Constant(0.)                           # Neumann heat flux
#u0_top     = Constant(surface_temp)                 # Dirichlet surface
u0_top     = Expression("A - x[1]*B", A=surface_temp, B=lapse_rate, \
                        degree=1, domain=mesh2d)     # Dirichlet surface
u0_right   = Constant(surface_temp)                  # Dirichlet terminus
dsN        = Measure("ds", subdomain_id=1, subdomain_data=boundary_parts)
dsN        = Measure("ds", subdomain_id=4, subdomain_data=boundary_parts)
dsN        = Measure("ds", subdomain_id=2, subdomain_data=boundary_parts)

# Define boundary condition
bc_top   = DirichletBC(V2d, u0_top  , boundary_parts, 3)
bc_right = DirichletBC(V2d, u0_right, boundary_parts, 4)
bcs = [bc_top, bc_right]

SRB       = True
ADVECTION = True

if SRB == True:
    # Test and trial functions
    u, v = TrialFunction(V2d), TestFunction(V2d)

    # Residual
    residual = dot(velocity, grad(u)) - div(diffusivity*grad(u)) - f

    # Galerkin variational problem
    # in some versions the dx requires the
    # domain specification (here mesh2d)
    if ADVECTION:
        # advection-diffusion problem
        F = v*dot(velocity, grad(u))*dx(mesh2d) + dot(grad(v), diffusivity*grad(u))*dx(mesh2d) \
            - f*v*dx(mesh2d) - g_bottom*v*dsN(2) - g_left*v*dsN(1) \
            #- g_right*v*dsN(4)

        # Add SUPG stabilisation terms
        vnorm = sqrt( dot(velocity, velocity) )
        h = CellSize(mesh2d)
        delta = h/(2.0*vnorm)
        F += delta * dot(velocity, grad(v)) * residual * dx(mesh2d)

    else:
        # diffusion-only  -  Poisson problem
        F = dot(grad(v), diffusivity*grad(u))*dx(mesh2d) \
            - f*v*dx(mesh2d) - g_bottom*v*dsN(2) - g_left*v*dsN(1) \
            #- g_right*v*dsN(4)

    # Create bilinear and linear forms
    a = lhs(F)
    L = rhs(F)

    # Solve
    temperature = Function(V2d)
    solve(a == L, temperature, bcs)

    save_file('temperature', temperature)
#    save_file('vnorm', project(vnorm, V2d) )



##########################################
######The Robin temperature solution######
Ts_Robin  = Function(V2d)
T_depth   = Function(V2d)

# because the expression for surface temperature
# is essentially only a function of z, the expression
# is applied to all nodes with the z from the thickness
# function which is constant at depth
# FIXME: there must be a better way!  
nvertices = mesh2d.ufl_cell().num_vertices()

# Set up a vertex_2_dof list
indices = [dofmap.tabulate_entity_dofs(0, i)[0] for i in range(nvertices)]

vertex_2_dof = dict()
[vertex_2_dof.update(dict(vd for vd in zip(cell.entities(0), \
                        dofmap.cell_dofs(cell.index())[indices]))) \
                        for cell in cells(mesh2d)]

# Get the vertex coordinates
X = mesh2d.coordinates()

# Set the vertex coordinate you want to modify
for xcoord, ycoord in X:
    vertex_idx = np.where((X == (xcoord,ycoord)).all(axis = 1))[0] 

    vertex_idx = vertex_idx[0]
    dof_idx = vertex_2_dof[vertex_idx]
    Ts_Robin.vector()[dof_idx] = u0_top(xcoord, thickness(xcoord, ycoord))

#save_file("temperature_Robin_s", Ts_Robin)

# the function that holds the vertical veloctiy
# at the surface and keeps it constant with depth
# is used as "accumulation rate" for the Robin solution
q_sqrd = abs(w_surface/2./(thickness+DOLFIN_EPS)/diffusivity)
q_term = sqrt(q_sqrd)

T_depth = Ts_Robin - GHF*sqrt(pi)/(2.*conductivity*q_term) * \
            ( erf(depth * q_term) - erf(thickness * q_term) )

print_min_max('Robin temperature', project(T_depth, V2d))

save_file("temperature_Robin", project(T_depth, V2d))
#save_file("depth",      depth)

difference = project(T_depth, V2d) - temperature
#plot(difference, interactive=True)
save_file("temperature_diff", project(difference, V2d))
