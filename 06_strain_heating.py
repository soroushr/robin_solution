import matplotlib.pyplot as plt
import matplotlib as mpl
import os, sys
from dolfin import *
import numpy as np
import constants
from mpmath import gammainc
from util import (
    solve_variational_form,
    SRB_solution,
    q_strain,
    gamma_as_peclet
)

plt.rc('font', family='TeX Gyre Schola')

FILENAME = os.path.splitext(__file__)[0]
#################################
## some constants for glacier ice
M_dot          = constants.M_dot
GHF            = constants.GHF
GAMMA          = constants.GAMMA
GAMMA_OPTIMAL  = constants.GAMMA_OPTIMAL
rate_factor    = constants.rate_factor
y2s            = constants.y2s

INTERVALS = 200
THICKNESS = 3000
#######################################################################

TAU_RANGE   = np.linspace(0, 60, 25)
THICKNESSES = np.linspace(2000, 4000, 3)
cmap = mpl.cm.spectral
rfs = [1e-8, 5e-8, 1e-7]
lss = [":", "-" , "--"]
lws = [2., 2., 2.]

def strain_diffs(STR):
    for i in [1,0,2]:
        for THK in THICKNESSES:
            diff_bed = []
            diff_avg = []
            mesh = IntervalMesh(INTERVALS, 0, THK)
            Q    = FunctionSpace(mesh, "CG", 1)
            for TAU in TAU_RANGE:
                if STR:
                    heat_strain = q_strain(thk=THK, tau=TAU, r_fac=rfs[i])
                    heat_total  = GHF + heat_strain
                else:
                    heat_total  = GHF                    

                gamma_opt = gamma_as_peclet(M_dot=M_dot, thk=THK)
                temperature_srb = SRB_solution(M_dot_srb=M_dot, mesh=mesh, Q=Q, thk=THK,\
                                                _gamma = gamma_opt, H_flux=heat_total)
                temperature_srb = temperature_srb.vector().array()[::-1]

                vz_SIA = Expression(("-A/4*(pow(1-x[0]/B,5)+5*x[0]/B-1)"), \
                                               A=M_dot, B=THK, domain=mesh, degree=5)
    #            vz_SIA = Expression(("-A*pow(x[0],C)/pow(B,C)"), A=M_dot,  \
    #                                   B=THK, C=gamma_opt, domain=mesh, degree=2)

                temperature_SIA_optimal = solve_variational_form(vz_SIA, r_fac=rfs[i], \
                                                tau=TAU, mesh=mesh, Q=Q, thk=THK, strain_heating=True)

                temperature_SIA_optimal = temperature_SIA_optimal.vector().array()[::-1]

                temp_diff_bed = temperature_srb[0]-temperature_SIA_optimal[0]
                diff_bed.append(temp_diff_bed)
                print 'difference is', temp_diff_bed
                print 'numerical basal temperature is', temperature_SIA_optimal[0]
                print 'difference in surface must be zero', temperature_srb[-1]-temperature_SIA_optimal[-1]
                temp_diff_avg = np.mean(temperature_SIA_optimal) - np.mean(temperature_srb)
                diff_avg.append(temp_diff_avg)

            clr = cmap( (THK-THICKNESSES[0])/(THICKNESSES[-1]-THICKNESSES[0]) - 0.15 )
            kwargs = {"color":clr, "lw":lws[i] , "ls":lss[i],  "alpha":0.8,  "label":"H=%i m"%float(THK)}
            plt.plot(TAU_RANGE, diff_bed, **kwargs)
            plt.grid(linestyle='dotted')

    plt.xlabel(r'driving stress $\tau_{dx}$ [kPa]')
    plt.ylabel(r'$T_{analytical}-T_{numerical}$ at the bed [K]')
    if STR:
        plt.legend(loc=2)
        plt.xlim([0,max(TAU_RANGE)])
        plt.ylim([-0.1,10])
        plt.savefig("results_%s/q-strain-effect.png"%FILENAME,dpi=300)
        plt.clf()
    else:
        plt.legend(loc=3)
        plt.xlim([0,max(TAU_RANGE)])
        plt.ylim([-10,0.1])
        plt.savefig("results_%s/q-strain-effect-no-Neumann.png"%FILENAME,dpi=300)
        plt.clf()

#strain_diffs(STR=True)
#strain_diffs(STR=False)
#sys.exit("saved the plots for basal temperature differences")
#--------------------------------------

rfs  = [1e-8, 5e-8, 1e-7]
rfs_str = ['10$^{-8}$',r'5$\times$10$^{-8}$','10$^{-7}$']
taus = [20., 40., 60.]
for rf,title in zip(rfs,rfs_str):
    fig = plt.figure(figsize=(14, 6))
    plt.suptitle('A=%s kPa$^{-3}$yr$^{-1}$'%title, fontsize=15, fontweight='bold')
    for idx,TAU in enumerate(taus):
        THK  = THICKNESS
        mesh = IntervalMesh(INTERVALS, 0, THK)
        Q    = FunctionSpace(mesh, "CG", 1)

        # optimal gamma for SRB solution
        gamma_opt = gamma_as_peclet(M_dot=M_dot, thk=THK)

        # vertical velocity of SIA
        vz_SIA = Expression(("-A/4*(pow(1-x[0]/B,5)+5*x[0]/B-1)"), \
                                       A=M_dot, B=THK, domain=mesh, degree=5)

        # SIA temperature
        temperature_SIA = solve_variational_form(vz_SIA, r_fac=rf, \
                             tau=TAU, mesh=mesh, Q=Q, thk=THK, strain_heating=True)

        # strain heating and total heat
        heat_strain = q_strain(thk=THK, tau=TAU, r_fac=rf)
        heat_total  = GHF + heat_strain

        temperature_srb = SRB_solution(M_dot_srb=M_dot, mesh=mesh, Q=Q, thk=THK,\
                                        _gamma = gamma_opt, H_flux=GHF)

        temperature_srb_str = SRB_solution(M_dot_srb=M_dot, mesh=mesh, Q=Q, thk=THK,\
                                        _gamma = gamma_opt, H_flux=heat_total)

        # temperatures as arrays
        temperature_SIA     = temperature_SIA.vector().array()[::-1]
        temperature_srb     = temperature_srb.vector().array()[::-1]
        temperature_srb_str = temperature_srb_str.vector().array()[::-1]

        coord = mesh.coordinates()[:,0]

        temp = 131+idx
        plt.subplot(temp)
        plt.plot(temperature_srb    , coord, color='k', lw=1.5, ls='--',\
                 alpha=0.7, label=r'analytical without strain heating')
        plt.plot(temperature_srb_str, coord, color='r', lw=2.5, ls='--',\
                 alpha=0.7, label=r'analytical with $G_{s}$')
        plt.plot(temperature_SIA    , coord, color='k', lw=1.5,\
                 alpha=1.0, label=r'numerical with $Q_{s}$')
        plt.xlabel('temperature [$^{\circ}$C]', fontsize=13.5)
        plt.grid(linestyle='dotted')
        plt.ylim([0,THICKNESS+50])
        plt.xlim([-32, 5])
        plt.xticks(size = 12.5)
        plt.yticks(size = 12.5)
        plt.title(r'$\tau_{dx}$=%.0f kPa'%TAU, fontsize=13.5)
        if temp==131:
            plt.legend(loc=1, prop={'size': 12})
            plt.ylabel('z [m]', fontsize=13.5)
        plt.tight_layout(rect=[0,0.03,1,0.95])

    plt.savefig("results_%s/profile_A_%.0e.png"%(FILENAME,rf),dpi=200)


#--------------------------------------
# comparing cumulative Qs vs. Gs
cumulative = False
if cumulative:
    tau_range = [20., 50., 80.]
    colors = ['g','b','r']
    for idx,tau in enumerate(tau_range):
        A = 5e-8
        thk_arr = np.linspace(THICKNESS,0,100)
        Qsz = [2./5.*A*THICKNESS*tau**4.*(1-z/THICKNESS)**5/y2s*1e6 for z in thk_arr]
        Gs  = 2./5.*A*THICKNESS*tau**4./y2s*1e6

        plt.plot(Qsz, thk_arr,color=colors[idx],linestyle='--')
        plt.axvline(x=Gs,color=colors[idx])
        plt.fill_betweenx(thk_arr, Qsz, Gs, facecolor=colors[idx], alpha=0.2,label=r'$\tau_{dx}=$%i kPa'%int(tau))
        plt.xlim([-5,80])
        plt.ylim([0,THICKNESS])
        plt.xlabel(r"strain heating [mW m$^{-2}$]")
        plt.ylabel(r"z [m]")
        plt.legend(loc=9)
        plt.grid(linestyle='dotted')
    plt.savefig("results_%s/cumulative_Gs.png"%FILENAME,dpi=300)

