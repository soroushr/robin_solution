"""
reproducing van der Veen (2013) figure 6.4 that is 
the analytical Robin solution without and numerical
Robin solution with strain heating.
parameters are
    - T_s = -35 C
    - GHF = 0.042 W/m2
    - tau = 100 kPa
    - thk = 3000 m
"""
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
from dolfin import *
import numpy as np
import constants
from util import (
    solve_variational_form,
    SRB_solution,
    Robin_solution,
    q_strain
)

plt.rc('font', family='TeX Gyre Schola')

FILENAME = os.path.splitext(__file__)[0]
#################################
## some constants for glacier ice
M_dot         = constants.M_dot
GAMMA         = constants.GAMMA
GAMMA_OPTIMAL = constants.GAMMA_OPTIMAL
rate_factor   = constants.rate_factor

THICKNESS = 3000.
INTERVALS = 200
T_SURFACE = -35
GHF       = 0.042
TAU       = 50.
#-----------------------------------
#   FENICS MESH AND FUNCTIONSPACE
#-----------------------------------
mesh = IntervalMesh(INTERVALS, 0, THICKNESS)
Q    = FunctionSpace(mesh, "CG", 1)

#------------------------------------------------
# define velocity and solve T for Robin solution,
# our solution, and FEniCS numerical solution
#------------------------------------------------
# the expression for vz_SIA_exact is from
# https://www.the-cryosphere-discuss.net/tc-2017-276/tc-2017-276.pdf
vz_1st         = Expression(("-A*pow(x[0],1)/pow(B,1)"), A=M_dot, B=THICKNESS, degree=1)
vz_SIA_exact   = Expression(("-A/4*(pow(1-x[0]/B,5)+5*x[0]/B-1)"), A=M_dot, B=THICKNESS, degree=5)

temperature_1st         = solve_variational_form(vz_1st, mesh=mesh, Q=Q, T_S=T_SURFACE, H_flux=GHF, strain_heating=True, tau=TAU)
temperature_SIA_exact   = solve_variational_form(vz_SIA_exact, mesh=mesh, Q=Q, T_S=T_SURFACE, H_flux=GHF)

temperature_robin = Robin_solution(M_dot_robin=M_dot, mesh=mesh, Q=Q, thk=THICKNESS, T_S=T_SURFACE, H_flux=GHF)

H_strain = q_strain(thk=THICKNESS, tau=TAU, r_fac=rate_factor)
temperature_robin_str = Robin_solution(M_dot_robin=M_dot, mesh=mesh,\
                                       Q=Q, thk=THICKNESS, T_S=T_SURFACE,\
                                       H_flux=GHF + H_strain)
temperature_srb   = SRB_solution(M_dot_srb=M_dot, mesh=mesh, Q=Q, thk=THICKNESS, T_S=T_SURFACE, H_flux=GHF)

#-----------------------------------
#              PLOTS
#-----------------------------------
temperature_1st         = temperature_1st        .vector().array()[::-1]
temperature_SIA_exact   = temperature_SIA_exact  .vector().array()[::-1]

temperature_robin     = temperature_robin.vector().array()[::-1]
temperature_robin_str = temperature_robin_str.vector().array()[::-1]
temperature_srb       = temperature_srb  .vector().array()[::-1]

coord = mesh.coordinates()[:,0]

##############################
#       PLOT PROBLEM
##############################
plt.plot(temperature_robin      , coord, 'red', ls='-'  , lw=2, alpha=0.7, \
            label=r'Robin solution $GHF$')
plt.plot(temperature_robin_str  , coord, 'green', ls='--' , lw=2, alpha=0.7, \
            label=r'Robin solution $G_{strain}+GHF$')
plt.plot(temperature_1st  , coord, 'black', ls='--'  , lw=2, alpha=0.7, \
         label=r'numerical solution $Q_{strain}$')
plt.title(r'analytical Robin vs. numerical solution $\tau$=%i kPa'%TAU)
plt.xlabel('temperature [$^{\circ}$C]');plt.ylabel('z [m]')
plt.grid(linestyle='dotted')
plt.ylim([0,THICKNESS+100])
plt.legend(loc=1)
plt.savefig("results_%s/problem-temperature_%i.png"%(FILENAME,TAU),dpi=300)
plt.clf()
