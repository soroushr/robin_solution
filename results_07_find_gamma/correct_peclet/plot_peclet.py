import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

plt.rc('font', family='TeX Gyre Schola')

infile_raw = np.loadtxt('outs_peclet.txt', delimiter=',', skiprows=1)

infile = infile_raw[ infile_raw[:,2] < 101 ]

#thk = infile[:,0]
#acc = infile[:,1]
pec = infile[:,2]
gam = infile[:,3]

#def func(x, a, b):
#    return a*x**b

def get_rsq(f, y, popt):

    ss_res = np.dot((y - f(pec, *popt)),(y - f(pec, *popt)))
    ymean = np.mean(y)
    ss_tot = np.dot((y-ymean),(y-ymean))
    return 1.-ss_res/ss_tot

def func(x, a, b):
    return a+b*np.log(x)
popt, pcov = curve_fit(func, pec, gam, maxfev=1000000)

rsq = get_rsq(func, gam, popt)
print rsq

kwargs = {'c':'r', 'alpha':0.8, 'lw':3, 'label':r'$r^2=$%.3f'%rsq}
# 'label':'$r^2=$%.3f'%rsq
#kwargs = {'c':'r', 'alpha':0.8, 'lw':3,
# 'label':'   best fit for $\gamma$\n\
#%.2f+%.3f*ln($Pe$)'%(popt[0],popt[1])}
plt.semilogx(sorted(pec), func(sorted(pec), *popt), **kwargs)
kwargs = {'s':40, 'alpha':0.8, 'edgecolors':'white',\
          'linewidths':.4, 'label':'simulations'}
plt.scatter(pec,gam,c='k', **kwargs)
#plt.grid(linestyle='dotted')
plt.grid(True,which="both",ls="dotted")
plt.xlabel(r"Peclet = $\frac {\dot M H}{K}$")
plt.ylabel(r'$\gamma_{+}$')
plt.xlim([0,100])
plt.ylim([1.45,1.60])
plt.gca().set_ylim(bottom=1.450)
plt.legend(loc=2)
plt.savefig('peclet-log.png',dpi=300)
plt.clf()
