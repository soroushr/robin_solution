import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

plt.rc('font', family='TeX Gyre Schola')

infile = np.loadtxt('outs_peclet.txt', delimiter=',', skiprows=1)

thk = infile[:,0]
acc = infile[:,1]
pec = infile[:,2]
gam = infile[:,3]

#def func(x, a, b):
#    return a*x**b
def func(x, a, b):
    return a+b*np.log(x)
popt, pcov = curve_fit(func, pec, gam, maxfev=1000000)

kwargs = {'c':'r', 'alpha':0.8, 'lw':3, 'label':'best fit for $\gamma$'}#\n\%.3f+%.3f*ln($x$)'%(popt[0],popt[1])}
plt.plot(sorted(pec), func(sorted(pec), *popt), **kwargs)
kwargs = {'s':40, 'alpha':0.8, 'edgecolors':'white',\
          'linewidths':.4, 'label':'simulations'}
plt.scatter(pec,gam,c='k', **kwargs)
plt.grid(linestyle='dotted')
plt.xlabel(r"$Pe=\frac {\dot M H}{K}$")
plt.ylabel(r'optimal $\gamma$')
plt.xlim([0,250])
plt.ylim([1.45,1.65])
#plt.gca().set_ylim(bottom=1.450)
plt.legend(loc=2)
plt.savefig('peclet.png',dpi=300)
plt.clf()
