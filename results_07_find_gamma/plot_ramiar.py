import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

plt.rc('font', family='TeX Gyre Schola')

infile = np.loadtxt('outs_ramiar_fix_H.txt', delimiter=',', skiprows=1)

thk = infile[:,0]
ghf = infile[:,1]
t_s = infile[:,2]
ram = infile[:,3]
gam = infile[:,4]

##def func(x, a, b):
##    return a*x**b
#def func(x, a, b):
#    return a+b*np.log(x)
#popt, pcov = curve_fit(func, pec, gam, maxfev=1000000)

#kwargs = {'c':'r', 'alpha':0.8, 'lw':3, 'label':'   best fit for $\gamma$\n\
#%.3f+%.3f*ln($x$)'%(popt[0],popt[1])}
#plt.plot(sorted(pec), func(sorted(pec), *popt), **kwargs)

kwargs = {'s':30, 'alpha':0.8, 'edgecolors':'white',\
          'linewidths':.4, 'label':'simulations'}
plt.scatter(abs(ram),gam,c='k', **kwargs)
plt.grid(linestyle='dotted')
plt.xlabel(r"$\Xi = \frac {H G} {\kappa (T_{ref}-T_s)}$")
plt.ylabel(r'optimal $\gamma$')
#plt.xlim([0,250])
plt.ylim([1.45,1.65])
#plt.gca().set_ylim(bottom=1.450)
#plt.legend(loc=2)
plt.savefig('ramiar_fix_H.png',dpi=300)
plt.clf()
