import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib as mpl
import os
from dolfin import *
import numpy as np
import constants
from mpmath import gammainc

FILENAME = os.path.splitext(__file__)[0]
#################################
## some constants for glacier ice
M_dot          = constants.M_dot
diffusivity    = constants.diffusivity
GHF            = constants.GHF
conductivity   = constants.conductivity
GAMMA          = constants.GAMMA
GAMMA_OPTIMAL  = constants.GAMMA_OPTIMAL
rate_factor    = constants.rate_factor
y2s            = constants.y2s
beta           = constants.beta
rho            = constants.rho
gravity        = constants.gravity
heat_capacity  = constants.heat_capacity

# some default 'typical' values
INTERVALS = 200
THICKNESS = 3000. # m
T_SURFACE = -30   # C
tau_dx    = 10    #kPa
#######################
## SIMULATION FUNCTIONS
#######################

#------------------------------
#     VARIATIONAL PROBLEM
#------------------------------
def solve_variational_form(vz, mesh, Q, tau=tau_dx, \
                       r_fac=rate_factor, thk=THICKNESS,\
                       strain_heating=False, T_S=T_SURFACE,\
                       H_flux=GHF, *args):

    """Solves the variational problem for a 1D advection diffusion
       problem with surface Dirichlet and basal Neumann

       Args:
            vz: dolfin expression for vertical velocity
            mesh: 1D dolfin mesh
            Q: function space of mesh object
            tau: driving stress for strain heating [kPa]
            r_fac: rate factor for strain heating [kPa-3 yr-1]
            thk: thickness [m]
            T_S: surface Dirichlet temperature [C]
            H_flux: heat flux at the bottom [W/m2]

       Return:
            temperature profile at depth [C]
    """
    # boundary markers
    boundary_parts = FacetFunction('size_t', mesh)
    bottom = AutoSubDomain(lambda x: near(x[0], 0.0))
    top    = AutoSubDomain(lambda x: near(x[0], thk))
    bottom.mark(boundary_parts, 2)
    top   .mark(boundary_parts, 3)

    # test and trial functions
    u   = TrialFunction(Q)
    psi = TestFunction(Q)

    # boundary values
    bottom_gradient = H_flux * (diffusivity/conductivity)

    # source term in W/m2
    # This is eq 6.29 in van der Veen (2013)
    class Source(Expression):
        def __init__(self, thk, tau,\
                    r_fac=rate_factor, *args, **kwargs):
            self.thk = thk
            self.tau = tau
            self.r_fac = r_fac
        def eval(self, values, x):
            dx = x[0]
            values[0] = 2.*self.r_fac*(1.-dx/self.thk+DOLFIN_EPS)**4. \
                          *self.tau**4./rho/heat_capacity*1e3
#                          *self.tau**4./y2s*1e3
#                          *self.tau**4.*(diffusivity/conductivity)/y2s*1e3
    if strain_heating:
        src = Source(thk=thk, tau=tau, r_fac=r_fac)
    else:
        src = Constant(0.)                  # no source term

    g_bottom = Constant(bottom_gradient)    # Neumann heat flux
    u0       = Constant(T_S)                # Dirichlet surface
    dsN      = Measure("ds", subdomain_id=2, subdomain_data=boundary_parts)

    # Galerkin variational problem
    # add SUPG stabilisation terms
    vnorm = sqrt(vz * vz)
    h = CellSize(mesh)

    # skewed test function
    psihat = psi + h/(2*vnorm) + vz * psi.dx(0)

    # advection-diffusion problem
    F = + psihat * (vz * u.dx(0)) * dx \
        + diffusivity*(psi.dx(0) * u.dx(0)) * dx \
        - src * psihat * dx \
        - g_bottom * psihat * dsN

    # bilinear and linear forms
    a = lhs(F)
    L = rhs(F)

    # define boundary condition
    bcs = DirichletBC(Q, u0, boundary_parts, 3)

    # Solve
    u = Function(Q)
    solve(a == L, u, bcs)

    return u

'''
#-----------------------------------
#         SRB SOLUTION
#-----------------------------------
def SRB_solution(M_dot_srb, mesh, Q, thk, T_S=T_SURFACE,\
                  _gamma=GAMMA_OPTIMAL, H_flux=GHF, *args):
    """Analytical solution from this study, that approximates
       the vertical velocity profile of SIA with a power  
       function of form M(z/H)^gamma instead of M(z/H)
       which is used in the Robin solution.

       Args:
            M_dot_srb: surface vertical velocity
                assumed to be the same as SMB [m/yr]
            mesh: 1D dolfin mesh
            Q: function space of mesh object
            thk: thickness [m]
            T_S: surface Dirichlet temperature [C]
            _gamma: exponent of the (z/H) in our solution
            H_flux: heat flux at the bottom [W/m2]

       Return:
            temperature profile at depth [C]
    """
    T_depth  = Function(Q)
    T_temp   = Function(Q)

    depth = project(Expression("x[0]"), Q)

    _lambda = M_dot_srb / (diffusivity * thk**_gamma)
    _zeta   = -_lambda/(_gamma+1.)

    cnst_subtractee = thk*(-_zeta*thk**(_gamma+1.))**(-1./(_gamma+1.)) *\
                    gammainc(1./(1.+_gamma), -_zeta*thk**(_gamma+1.) )

    cnst_factor = H_flux/(conductivity*(_gamma+1.))

    lin_depth = depth.compute_vertex_values(mesh)

    _lst = [cnst_factor *\
            (x*(-_zeta*x**(_gamma+1.))**(-1./(_gamma+1.)) *\
            gammainc(1./(1.+_gamma), -_zeta*x**(_gamma+1.) ) - \
            cnst_subtractee) for x in lin_depth+0.1]

    _lst = np.asarray([float(x) for x in _lst[::-1]])

    T_temp.vector()[:] = _lst
    T_depth = T_S + T_temp

    return project(T_depth, Q)
'''

#-----------------------------------
#         SRB SOLUTION
#-----------------------------------
def SRB_solution(M_dot_srb, mesh, Q, thk, T_S=T_SURFACE,\
                  _gamma=GAMMA_OPTIMAL, H_flux=GHF, *args):
    """Analytical solution from this study, that approximates
       the vertical velocity profile of SIA with a power  
       function of form M(z/H)^gamma instead of M(z/H)
       which is used in the Robin solution.

       Args:
            M_dot_srb: surface vertical velocity
                assumed to be the same as SMB [m/yr]
            mesh: 1D dolfin mesh
            Q: function space of mesh object
            thk: thickness [m]
            T_S: surface Dirichlet temperature [C]
            _gamma: exponent of the (z/H) in our solution
            H_flux: heat flux at the bottom [W/m2]

       Return:
            temperature profile at depth [C]
    """
    T_depth  = Function(Q)
    T_temp   = Function(Q)

    depth = project(Expression("x[0]"), Q)

    _lambda = M_dot_srb / (diffusivity * thk**_gamma)
    _zeta   = -_lambda/(_gamma+1.)

    cnst_subtractee = gammainc(1./(1.+_gamma), -_zeta*thk**(_gamma+1.) )

    cnst_factor = H_flux/(conductivity*(_gamma+1.)) *\
                 (-_zeta)**( -1./(_gamma+1.) )

    lin_depth = depth.compute_vertex_values(mesh)

    _lst = [cnst_factor *\
            (gammainc(1./(1.+_gamma), -_zeta*x**(_gamma+1.) ) - \
            cnst_subtractee) for x in lin_depth+0.1]

    _lst = np.asarray([float(x) for x in _lst[::-1]])

    T_temp.vector()[:] = _lst
    T_depth = T_S + T_temp

    return project(T_depth, Q)

#----------------------------------
#    G-PMP from SRB SOLUTION
#----------------------------------
def find_Gpmp(Ts, thk, M_dot_srb=M_dot, *args):
    """Estimates the GHF required to reach the bed to
       the pressure melting point (hence, Gpmp) by
       setting the Tb = Tpmp and solving for G in 
       the analytical solution.

       Args:
            T_S: surface Dirichlet temperature [C]
            thk: thickness [m]
            M_dot_srb: surface vertical velocity
                assumed to be the same as SMB [m/yr]
            mesh: 1D dolfin mesh

       Return:
            Gpmp in [mW/m2]
    """

    Tpmp =  - rho*gravity*thk*beta
    peclet = M_dot_srb * thk / diffusivity
    _gamma = 1.39 + 0.044 * np.log(peclet)
    _lambda = M_dot_srb / (diffusivity * thk**_gamma)
    _zeta   = -_lambda/(_gamma+1.)

    cnst_dividee = gammainc(1./(1.+_gamma), 0) - gammainc(1./(1.+_gamma), -_zeta*thk**(_gamma+1.) )

    G_pmp = (Tpmp-Ts) * conductivity * (_gamma+1.) * (-_zeta) ** ( 1./(1.+_gamma) ) / cnst_dividee

    return G_pmp*1000.
#-----------------------------------
#         ROBIN SOLUTION
#-----------------------------------
def Robin_solution(M_dot_robin, mesh, Q, thk,\
                   H_flux=GHF, T_S=T_SURFACE, *args):
    """Robin 1955 analytical solution, that approximates
       the vertical velocity profile as M(z/H).

       Args:
            M_dot_robin: surface vertical velocity
                assumed to be the same as SMB [m/yr]
            mesh: 1D dolfin mesh
            Q: function space of mesh object
            thk: thickness [m]
            T_S: surface Dirichlet temperature [C]
            H_flux: heat flux at the bottom [W/m2]

       Return:
            temperature profile at depth [C]
    """
    T_depth  = Function(Q)

    depth = project(Expression("x[0]"), Q)
    q_sqrd = abs(M_dot_robin/2./(thk+DOLFIN_EPS)/diffusivity)
    q_term = sqrt(q_sqrd)

    T_depth = T_S - H_flux*sqrt(pi)/(2. * conductivity * q_term) * \
                ( erf(depth * q_term) - erf(thk * q_term) )

    return project(T_depth, Q)

#-----------------------------------
#   INTEGRAL OF STRAIN HEATING
#   AS NEUMANN IN W/m2
#-----------------------------------
def q_strain(thk=THICKNESS, tau=tau_dx,\
            r_fac=rate_factor, *args):
    """Strain heating that is integrated at depth
       as a heat sum to be added to the GHF at the
       bed as the Neumann condition. This is the
       integral of eq 6.29 in van der Veen (2013)

       Args:
            thk: thickness [m]
            tau: driving stress for strain heating [kPa]
            r_fac: rate factor for strain heating [kPa-3 yr-1]

       Return:
            scalar for heat [W/m2]
    """
    heat = 2./5.*r_fac*thk*tau**4./y2s*1e3

    return heat

#-----------------------------------
#   EQUIVALENT TO ADJOINT PROBLEM
#-----------------------------------
def find_gamma(M_dot_srb, THK, gamma_init=GAMMA_OPTIMAL,\
               T_S=T_SURFACE, H_flux=GHF, TOL=0.01, *args):
    """Finds the optimal gamma given a range
       of other input parameters. The temperature
       solution from variational solver remains the
       same, while the gamma power is iteratively
       adjusted until the difference between our
       solution and numerical solution is less than
       a defined tolerance. The default for tolerance
       is 0.01.

       Args:
            M_dot_srb: surface vertical velocity
                assumed to be the same as SMB [m/yr]
            THK: thickness [m]
            gamma_init: initial value for gamma before iteration.
                it is better to set it to GAMMA_OPTIMAL
            T_S: surface Dirichlet temperature [C]
            H_flux: heat flux at the bottom [W/m2]
            TOL: tolerance for termination of iteration

       Return:
            optimal gamma

       Usage:
            gamma_opt = find_gamma(*args)
            print gamma_opt

    """
    mesh = IntervalMesh(INTERVALS, 0, THK)
    Q    = FunctionSpace(mesh, "CG", 1)

    coord = mesh.coordinates()[:,0]

    vz_SIA = Expression(("-A/4*(pow(1-x[0]/B,5)+5*x[0]/B-1)"), \
                           A=M_dot_srb, B=THK, domain=mesh, degree=5)
    bed_full = solve_variational_form(vz=vz_SIA, mesh=mesh, Q=Q,\
                                      thk=THK, T_S=T_S, H_flux=H_flux)
    bed_srb  = SRB_solution(_gamma=gamma_init, M_dot_srb=M_dot_srb,\
                            mesh=mesh, Q=Q, thk=THK, T_S=T_S, H_flux=H_flux)

    bed_srb  = bed_srb .vector().array()[::-1]
    bed_full = bed_full.vector().array()[::-1]

    Tb_diff = bed_srb[0]-bed_full[0]
    while abs(Tb_diff)>TOL:
        if Tb_diff>TOL:
            gamma_init = gamma_init*0.99
            bed_srb  = SRB_solution(_gamma=gamma_init, M_dot_srb=M_dot_srb,\
                                    mesh=mesh, Q=Q, thk=THK, T_S=T_S, H_flux=H_flux)
            bed_srb  = bed_srb .vector().array()[::-1]
            print ':::::::::::'
            print ' GAMMA is %f - too hot!  '%gamma_init
            print ':::::::::::'

        elif Tb_diff<-TOL:
            gamma_init = gamma_init*1.01
            bed_srb  = SRB_solution(_gamma=gamma_init, M_dot_srb=M_dot_srb,\
                            mesh=mesh, Q=Q, thk=THK, T_S=T_S, H_flux=H_flux)
            bed_srb  = bed_srb .vector().array()[::-1]
            print ':::::::::::'
            print ' GAMMA is %f - too cold!  '%gamma_init
            print ':::::::::::'
        
        Tb_diff = bed_srb[0]-bed_full[0]
        print bed_srb[0]
        print bed_full[0]

    return gamma_init


#-----------------------------------
#   GAMMA AS A FUNCTION OF PECLET
#-----------------------------------
def gamma_as_peclet(M_dot, thk):
    """The curve-fitting result for finding the 
       optimal gamma as a function of Peclet number.
       The expression is obtained by running the 
       function `find_gamma`.

       Args:
            M_dot: surface vertical velocity
                assumed to be the same as SMB [m/yr]
            thk: thickness [m]

       Return:
            optimal gamma
    """
    peclet = M_dot*thk/diffusivity
    OPT = 1.390 + 0.044*np.log(peclet)
    
    return OPT

###############################
## I/O FUNCTIONS
###############################
def print_min_max(varname, variable):
    maxx = variable.vector().max()
    minn = variable.vector().min()
    print ':::::::::::::::::::::::::::::::'
    print '     max %s is %f' %(varname, maxx)
    print '     min %s is %f' %(varname, minn)
    print ':::::::::::::::::::::::::::::::'

def save_xml(varname, variable, fname, *args):
    """
    save variable as xml to load in fenics later
    """
    file = File("results_%s/%s.xml"%(fname,varname))
    file << variable
    print '%s saved to xml file'%varname

def save_pvd(varname, variable, fname, *args):
    """
    save variable as pvd for visualization
    """
    file = File("results_%s/%s.pvd"%(fname,varname))
    file << variable
    print '%s saved to pvd file'%varname
    print_min_max(varname, variable)

def save_xml_and_pvd(varname, variable, fname, *args):
    """
    save both xml and pvd
    """
    save_xml(varname, variable, fname)
    save_pvd(varname, variable, fname)

#-----------------------------------
#   USE A SUBSET OF A COLORMAP
#-----------------------------------
# from https://stackoverflow.com/questions/18926031/how-to-extract-a-subset-of-a-colormap-as-a-new-colormap-in-matplotlib
def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=10, *args):
    new_cmap = colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap
