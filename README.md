Quantifying the errors associated with the Robin (1955) temperature solution by considering the effect of horizontal heat advection.

A numerical advection diffusion scheme is solved with FEniCS, first in 1D to reproduce the Robin solution. The numerical solution in 2D is then used to
investigate the effect of advective heat transfers.

See [this paper](https://agupubs.onlinelibrary.wiley.com/doi/abs/10.1029/2018JF004774) for details.